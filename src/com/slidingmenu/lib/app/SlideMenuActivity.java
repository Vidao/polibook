/*
 * Name: $RCSfile: MainMenuBaseActivity.java,v $ Version: $Revision: 1.0 $ Date:
 * $Date: Oct 26, 2012 10:58:19 AM $ Copyright (C) 2012 COMPANY_NAME, Inc. All
 * rights reserved.
 */

package com.slidingmenu.lib.app;


import com.netviet.polibook.R;
import com.netviet.polibook.view.SlideMenuFragment;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


/**
 * This is Base Menu Activity. It is used for all activity with sliding menu
 */
public class SlideMenuActivity extends SlidingFragmentActivity {
    protected static final int SELECTION_LOCATION_ACTIVITY_CODE = 0;
    protected static final int JB_ADD_SONG_QUEUE_ACTIVITY_CODE = 1;
    protected static final int SLIDE_MENU_HOME = 0x01;
    protected static final int SLIDE_MENU_PUBLIC = 0x02;

    /**
     * Global values
     */
    protected String tag;

    protected LinearLayout contentLayout;

    protected ImageView menuButton;
    protected ImageView rightButton;
    
    protected TextView titleText;
    protected static LayoutInflater inflater;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tag = getClass().getSimpleName();

        /* Set the Above View */
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.frame_content);
        /* Set the Behind View */
        setBehindContentView(R.layout.frame_menu);
        FragmentTransaction t = this.getSupportFragmentManager().beginTransaction();
        menuListContent = new SlideMenuFragment();
        t.add(R.id.frameMenu, menuListContent);
        t.commit();
        /* Customize the SlidingMenu */
        this.setSlidingActionBarEnabled(true);
        getSlidingMenu().setShadowWidthRes(R.dimen.shadow_width);
        getSlidingMenu().setShadowDrawable(R.drawable.shadow);
        getSlidingMenu().setBehindOffsetRes(R.dimen.actionbar_home_width);
        getSlidingMenu().setBehindScrollScale(0.25f);

        /* Initial UI objects and implement its action */
        initUI();
    }

   

    protected SlideMenuFragment menuListContent;

    /* Initial UI objects and implement its action */
    private void initUI() {
        inflater = LayoutInflater.from(getApplicationContext());
        contentLayout = (LinearLayout) findViewById(R.id.frameContent);
        menuButton = (ImageView) findViewById(R.id.iv_mainmenu);
        menuButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (getSlidingMenu().isBehindShowing()) {
                    getSlidingMenu().showAbove();
                } else {
                    getSlidingMenu().showBehind();
                }
            }
        });
        rightButton = (ImageView) findViewById(R.id.btnRight);
        titleText = (TextView) findViewById(R.id.statusNavigationTxt);
    }

    /**
     * Add layout content to screen. This function will be call in inheritanced
     * class
     *
     * @param contentFrame
     */
    protected void addContentFrame(View contentFrame) {
        contentLayout.addView(contentFrame);
    }

}