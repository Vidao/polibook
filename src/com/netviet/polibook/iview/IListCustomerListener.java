package com.netviet.polibook.iview;

public interface IListCustomerListener {
	public void onIDClicked();

	public void onAgeClicked();

	public void onNameClicked();
}
