package com.netviet.polibook.iview;

public interface ICustomerView {

	public void setID(String id);

	public void setAge(int age);

	public void setName(String name);

	public String getID();

	public int getAge();

	public String getName();
}
