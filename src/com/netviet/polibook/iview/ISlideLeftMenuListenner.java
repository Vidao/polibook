package com.netviet.polibook.iview;

public interface ISlideLeftMenuListenner {
	
	public void profileSelectedListener();

    public void listCustomerSelectedListener();

    public void homeSelectedListener();

    public void preferenceSelectedListener();
    
    public void quitSelectedListener();
}
