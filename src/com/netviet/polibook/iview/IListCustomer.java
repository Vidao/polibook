package com.netviet.polibook.iview;

import com.netviet.polibook.model.CustomerModel;

public interface IListCustomer {
	public void addCustomer(CustomerModel model);
	public void removeCustomer(int index);
	public void removeAll();
}
