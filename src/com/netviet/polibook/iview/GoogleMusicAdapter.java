package com.netviet.polibook.iview;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.netviet.polibook.view.BookFragment;
import com.netviet.polibook.view.HomeFragments;

public class GoogleMusicAdapter extends FragmentPagerAdapter {

	private String[] CONTENT;

	public GoogleMusicAdapter(FragmentManager fm, String[] content) {
		super(fm);
		this.CONTENT = content;
	}

	@Override
	public Fragment getItem(int position) {
		if (position != 1) {
			return BookFragment.newInstance(CONTENT[position % CONTENT.length]);
		}else{
			return HomeFragments.newInstance(CONTENT[position % CONTENT.length]);
		}
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return CONTENT[position % CONTENT.length].toUpperCase();
	}

	@Override
	public int getCount() {
		return CONTENT.length;
	}

}
