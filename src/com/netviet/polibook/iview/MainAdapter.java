package com.netviet.polibook.iview;

import com.netviet.polibook.R;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

public class MainAdapter extends PagerAdapter {

	Context context;
	private String[] page_titles = new String[] { "Home", "Me", "Apps",
			"Android", "About", "more", "more2", "more3", "more4", "more3", "more4" };
	private String[] desc = new String[] {
			"This is the homepage the first one you will see.",
			"I'm pretty much me for now I run this really cool blog you should check it out at mycodeandlife.wordpress.com",
			"I build appps mostly for fun. If you ever want an app just holla",
			"This is the android section",
			"This blog is my journal through life in code and development",
			"this is vidp more", "this is vidp more 2", "this is vidp more",
			"this is vidp more 2", "more3", "more4" };

	public MainAdapter(Context context) {
		this.context = context;
	}

	public String[] getPage_titles() {
		return page_titles;
	}

	public void setPage_titles(String[] page_titles) {
		this.page_titles = page_titles;
	}

	public String[] getDesc() {
		return desc;
	}

	public void setDesc(String[] desc) {
		this.desc = desc;
	}

	// This is the number of pages -- 5
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return page_titles.length;
	}

	@Override
	public boolean isViewFromObject(View v, Object o) {
		// TODO Auto-generated method stub
		return v.equals(o);
	}

	// This is the title of the page that will apppear on the "tab"
	public CharSequence getPageTitle(int position) {
		return page_titles[position];
	}

	// This is where all the magic happen
	public Object instantiateItem(View pager, int position) {
		final LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = inflater.inflate(R.layout.page, null, false);

		TextView title = (TextView) v.findViewById(R.id.tvTitle);
		TextView description = (TextView) v.findViewById(R.id.tvdesc);

		title.setText(page_titles[position]);
		description.setText(desc[position]);

		// This is very important
		((ViewPager) pager).addView(v, 0);

		return v;
	}

	@Override
	public void destroyItem(View pager, int position, Object view) {
		((ViewPager) pager).removeView((View) view);
	}

	@Override
	public void finishUpdate(View view) {
	}

	@Override
	public void restoreState(Parcelable p, ClassLoader c) {
	}

	@Override
	public Parcelable saveState() {
		return null;
	}

	@Override
	public void startUpdate(View view) {
	}

}
