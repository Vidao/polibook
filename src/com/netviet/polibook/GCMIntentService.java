package com.netviet.polibook;

import java.util.Stack;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.netviet.polibook.func.MainActivity;

public class GCMIntentService extends IntentService {

	public static final int NOTIFICATION_ID = 1;
	private NotificationManager mNotificationManager;
	NotificationCompat.Builder builder;
	public static final String TAG = "GCMLOG";

	public GCMIntentService() {
		super(ApplicationClass.PROJECT_NUMBER);
	}

	public static Stack<String> notification_queue = new Stack<String>();

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		// The getMessageType() intent parameter must be the intent you received
		// in your BroadcastReceiver.
		String messageType = gcm.getMessageType(intent);
		if (!extras.isEmpty()) { // has effect of unparcelling Bundle
			/*
			 * Filter messages based on message type. Since it is likely that
			 * GCM will be extended in the future with new message types, just
			 * ignore any message types you're not interested in, or that you
			 * don't recognize.
			 */
			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
					.equals(messageType)) {
				sendNotification("Send error: " + extras.toString());
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
					.equals(messageType)) {
				sendNotification("Deleted messages on server: "
						+ extras.toString());
				// If it's a regular GCM message, do some work.

			} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
					.equals(messageType)) {
				String _content = intent.getExtras().getString("DATA");
				if (_content == null || _content.length() < 3) {
					_content = intent.getExtras().getString("MESSAGE");
				}
				sendNotification(_content);
			}
		}
		// Release the wake lock provided by the WakefulBroadcastReceiver.
		GcmBroadcastReceiver.completeWakefulIntent(intent);
	}

	public static final String PROCESS_RESPONSE = "com.as400samplecode.intent.action.PROCESS_RESPONSE";

	// Put the message into a notification and post it.
	// This is just one simple example of what you might choose to do with
	// a GCM message.
	private void sendNotification(String msg) {
		if (msg == null) {
			return;
		}
		mNotificationManager = (NotificationManager) this
				.getSystemService(Context.NOTIFICATION_SERVICE);

		String _contentMessage = "";
		int _notificationType = -1;// 1makeFr;2FreidnMess;3add2group;4:deleteFgroup;5Group:del
		String _from = "";
		String _link = "";
		JSONObject _notifyContent = null;
		// parse content:
		try {
			_notifyContent = new JSONObject(msg);
			_from = _notifyContent.getString("fromName");
			_notificationType = _notifyContent.getInt("type");
			_contentMessage = _notifyContent.getString("message");
			_contentMessage = _from + ": " + _contentMessage;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}

		Intent intent = null;
		Bundle _bundle = new Bundle();
		switch (_notificationType) {
		case 1:
//			intent = new Intent(this, FriendRequestActivity.class);
			break;
		case 2:
//			intent = new Intent(this, ConversationActivity.class);
			String _friendID;
			String _friendAvatar;
			try {
				_friendID = _notifyContent.getString("fromID");
				_friendAvatar = _notifyContent.getString("fromAvatar");
				_link = _notifyContent.getString("link");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return;
			}
			
			_bundle.putString("FRIENDNAME", _from);
			_bundle.putString("FRIENDAVATAR", _friendAvatar);
			_bundle.putInt("FRIENDID", Integer.parseInt(_friendID));
			intent.putExtras(_bundle);
			break;
		case 6:
//			intent = new Intent(this, ConversationGroupActivity.class);
			try {
				int _groupID = _notifyContent.getInt("groupID");
				int _ownerID = _notifyContent.getInt("ownerID");
				String _groupName = _notifyContent.getString("groupName");
				_bundle.putString("GROUPNAME", _groupName);
				_bundle.putInt("GROUPID", _groupID);
				_bundle.putInt("OWNERID", _ownerID);
				intent.putExtras(_bundle);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 3:
//			intent = new Intent(this, GroupActivity.class);
			break;

		default:
			break;
		}

		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);

		PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
				intent, PendingIntent.FLAG_UPDATE_CURRENT);
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				this)
				.setSmallIcon(R.drawable.ic_home)
				.setContentTitle("LOVELYRING:")
				.setStyle(
						new NotificationCompat.BigTextStyle()
								.bigText(_contentMessage))
				.setContentText(_contentMessage);

		mBuilder.setContentIntent(contentIntent);
		mBuilder.setAutoCancel(true);
		Notification myNotification = mBuilder.build();
		myNotification.defaults |= Notification.DEFAULT_SOUND;
		myNotification.defaults |= Notification.DEFAULT_VIBRATE;
		myNotification.defaults |= Notification.DEFAULT_LIGHTS;

		mNotificationManager.notify(NOTIFICATION_ID, myNotification);
		Intent broadCastIntent = new Intent(ApplicationClass.DISPLAY_MESSAGE_ACTION);
		broadCastIntent.putExtra("message", msg);
		sendBroadcast(broadCastIntent);
	}

}
