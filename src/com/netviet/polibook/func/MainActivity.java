package com.netviet.polibook.func;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.netviet.polibook.ApplicationClass;
import com.netviet.polibook.R;
import com.netviet.polibook.iview.GoogleMusicAdapter;
import com.netviet.polibook.iview.ISlideLeftMenuListenner;
import com.netviet.polibook.iview.MainAdapter;
import com.netviet.polibook.net.MyAsyncHttpPost;
import com.netviet.polibook.net.MyAsyncHttpResponseProcess;
import com.netviet.polibook.utility.DialogManager;
import com.netviet.polibook.utility.ParameterFactory;
import com.netviet.polibook.utility.PreferencesManager;
import com.netviet.polibook.utility.ToastManager;
import com.netviet.polibook.view.BookFragment;
import com.viewpagerindicator.TabPageIndicator;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends BaseSlideActivity implements
		ISlideLeftMenuListenner, OnClickListener {
	// for regist GCM at first time;
	public static final String EXTRA_MESSAGE = "message";
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	static final String TAG = "GCMLOG";
	String SENDER_ID = ApplicationClass.PROJECT_NUMBER;
	GoogleCloudMessaging gcm;
	AtomicInteger msgId = new AtomicInteger();
	String regid;
	private MyAsyncHttpPost asyncUpdateDeviceToken = null;
	private MyAsyncHttpPost asyncSignUp = null;
	private MyAsyncHttpPost asyncLogin = null;
	private final String[] CONTENT = new String[] { "Thể loại",
			"Trang chủ", "Sách mới", "Tải nhiều", "Miễn phí", "Mua nhiều",
			"Sách của tôi" };

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		self = MainActivity.this;
		// View view = inflater.inflate(R.layout.activity_main, null);
		setContentView(R.layout.activity_main);
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		// display.getSize(size);
		// LovelyRing.SCREEN_WIDTH = size.x < size.y ? size.x : size.y;
		ApplicationClass.SCREEN_WIDTH = size.x;
		ApplicationClass.SCREEN_HEIGH = size.y;
		int statusBarHeigh = getStatusBarHeight();
		ApplicationClass.SCREEN_HEIGH -= statusBarHeigh;
		// addContentFrame(view);
		initUI();

	}


	public int getStatusBarHeight() {
		int statusBarHeight = 0;

		if (!hasOnScreenSystemBar()) {
			int resourceId = getResources().getIdentifier("status_bar_height",
					"dimen", "android");
			if (resourceId > 0) {
				statusBarHeight = getResources().getDimensionPixelSize(
						resourceId);
			}
		}

		return statusBarHeight;
	}

	private boolean hasOnScreenSystemBar() {
		Display display = getWindowManager().getDefaultDisplay();
		int rawDisplayHeight = 0;
		try {
			Method getRawHeight = Display.class.getMethod("getRawHeight");
			rawDisplayHeight = (Integer) getRawHeight.invoke(display);
		} catch (Exception ex) {
		}

		int UIRequestedHeight = ApplicationClass.SCREEN_HEIGH;

		return rawDisplayHeight - UIRequestedHeight > 0;
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if (asyncUpdateDeviceToken != null) {
			asyncUpdateDeviceToken.setFlagStop(true);
		}
		if (asyncSignUp != null) {
			asyncSignUp.setFlagStop(true);
		}
		if (asyncLogin != null) {
			asyncLogin.setFlagStop(true);
		}
	}

	/**
	 * Registers the application with GCM servers asynchronously.
	 * <p>
	 * Stores the registration ID and app versionCode in the application's
	 * shared preferences.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void registerInBackground() {
		new AsyncTask() {

			@Override
			protected void onPostExecute(Object result) {
				Log.d(TAG, result.toString());
			};

			@Override
			protected Object doInBackground(Object... arg0) {
				// TODO Auto-generated method stub
				String msg = "";
				try {
					if (gcm == null) {
						gcm = GoogleCloudMessaging.getInstance(self);
					}
					regid = gcm.register(SENDER_ID);
					msg = "Device registered, registration ID=" + regid;

					// You should send the registration ID to your server over
					// HTTP,
					// so it can use GCM/HTTP or CCS to send messages to your
					// app.
					// The request to your server should be authenticated if
					// your app
					// is using accounts.
					sendRegistrationIdToBackend(regid);

					// For this demo: we don't need to send it because the
					// device
					// will send upstream messages to a server that echo back
					// the
					// message using the 'from' address in the message.

					// Persist the regID - no need to register again.
					storeRegistrationId(self, regid);
				} catch (IOException ex) {
					msg = "Error :" + ex.getMessage();
					// If there is an error, don't just keep trying to register.
					// Require the user to click a button again, or perform
					// exponential back-off.
				}
				return msg;
			}
		}.execute(null, null, null);

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		// unregisterReceiver(mHandleMessageReceiver);
		super.onDestroy();
	}

	private void sendRegistrationIdToBackend(final String deviceToken) {
		// send registID to server here;
		PreferencesManager _pref = PreferencesManager.getInstance(self);
		String userName = _pref
				.getStringValue(PreferencesManager.PREF_USER_NAME);
		String userToken = _pref
				.getStringValue(PreferencesManager.PREF_USER_TOKEN);

		List<NameValuePair> params = ParameterFactory.deviceTokenParam(
				userName, userToken, deviceToken);
		asyncUpdateDeviceToken = new MyAsyncHttpPost(self,
				new MyAsyncHttpResponseProcess(self) {
					@Override
					public void before() {
						// TODO Auto-generated method stub
						super.before();
					}

					@Override
					public void processIfResponseSuccess(String response) {
						PreferencesManager _pref = PreferencesManager
								.getInstance(self);
						_pref.putBooleanValue(
								PreferencesManager.PREF_IS_SEND_TOKEN, true);
					}

					@Override
					public void processIfResponseFail(String message) {
						// failed
						// DialogManager.alert(self, "Sign Up", message);
					}

					@Override
					public void processIfServerError() {
						// TODO Auto-generated method stub
						super.processIfServerError();
					}
				}, params);
		asyncUpdateDeviceToken.execute(ApplicationClass.URL_SET_DEVICE_TOKEN);

	}

	/**
	 * Stores the registration ID and app versionCode in the application's
	 * {@code SharedPreferences}.
	 * 
	 * @param context
	 *            application's context.
	 * @param regId
	 *            registration ID
	 */
	private void storeRegistrationId(Context context, String regId) {
		final PreferencesManager prefs = PreferencesManager.getInstance(self);
		int appVersion = ApplicationClass.getAppVersion(context);
		Log.i(TAG, "Saving regId on app version " + appVersion);

		prefs.putStringValue(PreferencesManager.PREF_PROPERTY_REG_ID, regId);
		prefs.putIntValue(PreferencesManager.PREF_PROPERTY_APP_VERSION,
				appVersion);
	}

	private String getRegistrationId(Context context) {
		final PreferencesManager prefs = PreferencesManager.getInstance(self);
		String registrationId = prefs.getStringValue(
				PreferencesManager.PREF_PROPERTY_REG_ID, "");
		if (registrationId.isEmpty()) {
			return "";
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = prefs
				.getIntValue(PreferencesManager.PREF_PROPERTY_APP_VERSION,
						Integer.MIN_VALUE);
		int currentVersion = ApplicationClass.getAppVersion(context);
		if (registeredVersion != currentVersion) {
			Log.i(TAG, "App version changed.");
			prefs.putIntValue(PreferencesManager.PREF_PROPERTY_APP_VERSION,
					currentVersion);
			return "";
		}
		return registrationId;
	}

	private boolean isLogin = false;
	private String userName;

	private void initUI() {
		// MainAdapter mAdapter = new MainAdapter(this);
		FragmentPagerAdapter mAdapter = new GoogleMusicAdapter(
				getSupportFragmentManager(),CONTENT);
		// instantiate the Views
		ViewPager mPager = (ViewPager) findViewById(R.id.pager);
		mPager.setAdapter(mAdapter);
		TabPageIndicator mIndicator = (TabPageIndicator) findViewById(R.id.indicator);
		mIndicator.setFadingEdgeLength(50);
		mIndicator.setViewPager(mPager);
		ImageView _imageView = (ImageView) findViewById(R.id.iv_mainmenu);
		_imageView.setOnClickListener(this);

	}

	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				DialogManager.alert(self, R.string.gcm_play_service_error);
			}
			return false;
		}
		return true;
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		checkPlayServices();
		PreferencesManager _pref = PreferencesManager.getInstance(self);
		userName = _pref.getStringValue(PreferencesManager.PREF_USER_NAME);
		if (userName == null || userName.length() < 6) {
			// bt_login.setText("Login");
			isLogin = false;
		} else {
			// bt_login.setText("Logout");
			isLogin = true;
			menuListContent.setContext(self);
		}

	}

	@Override
	public void profileSelectedListener() {
		toggle();
	}

	@Override
	public void listCustomerSelectedListener() {
		toggle();
	}

	@Override
	public void homeSelectedListener() {
		toggle();
	}

	@Override
	public void preferenceSelectedListener() {
		showToastMessage("Clicked on preference!");
		toggle();
	}

	private void showToastMessage(String message) {
		Toast.makeText(self, message, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void quitSelectedListener() {
		finish();
	}

	@Override
	public void onClick(View v) {
		 int _viewID = v.getId();
		 switch (_viewID) {
		case R.id.iv_mainmenu:
			toggle();
			break;

		default:
			break;
		}
	}

	private void logout() {
		PreferencesManager _pref = PreferencesManager.getInstance(self);
		_pref.putStringValue(PreferencesManager.PREF_USER_NAME, "");
		_pref.putStringValue(PreferencesManager.PREF_FACEBOOK_USER_TOKEN, "");
		// bt_login.setText("Login");
		isLogin = false;
	}

	private void processSign() {
		if (isLogin) {
			DialogManager.showConfirmDialog(self, "Do you want to logout",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							logout();
						}
					}, null);
		} else {
		}
	}

	final private Handler dialogHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {

		};
	};

	private void signup(final String userName, String passWord,
			String phoneNumber) {
		List<NameValuePair> params = ParameterFactory.signUpParam(userName,
				passWord, phoneNumber);
		asyncSignUp = new MyAsyncHttpPost(self, new MyAsyncHttpResponseProcess(
				self) {

			@Override
			public void before() {
				// TODO Auto-generated method stub
				super.before();
				showProgressDialog(false);
			}

			@Override
			public void processIfResponseSuccess(String response) {
				parseSignUp(response);

			}

			@Override
			public void processIfResponseFail(String message) {
				// failed
				DialogManager.alert(self, "Sign Up", message);
			}
		}, params);

		asyncSignUp.execute(ApplicationClass.URL_SIGN_UP);
	}

	private void parseSignUp(String response) {
		try {
			JSONObject _jsResponse = new JSONObject(response);
			String _errorCode = _jsResponse.getString("ERROR_CODE");
			String _message = _jsResponse.getString("MESSAGE");
			if (_errorCode.equals("0")) {
				// signUpDialog.dismiss();
			} else {
				ToastManager.showLongToastMessage(self, _message);
				return;
			}

			int _userID = _jsResponse.getInt("ID");
			ToastManager.showLongToastMessage(self, _message);
			if ("0".equals(_errorCode)) {
				String _userToken = _jsResponse.getString("Token");
				// mark isLogin == true
				PreferencesManager pref = PreferencesManager.getInstance(self);
				pref.putBooleanValue(PreferencesManager.PREF_IS_SAVE_ACCOUNT,
						true);
				pref.putStringValue(PreferencesManager.PREF_USER_NAME, userName);
				pref.putStringValue(PreferencesManager.PREF_USER_TOKEN,
						_userToken);
				pref.putIntValue(PreferencesManager.PREF_USER_ID, _userID);
				// update device token;
				getDeviceToken();
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void login(final String userName, String passWord,
			final boolean isSaving) {
		List<NameValuePair> params = ParameterFactory.loginParam(userName,
				passWord);
		asyncLogin = new MyAsyncHttpPost(self, new MyAsyncHttpResponseProcess(
				self) {

			@Override
			public void before() {
				// TODO Auto-generated method stub
				super.before();
				showProgressDialog(false);
			}

			@Override
			public void processIfResponseSuccess(String response) {
				parseLogin(response, isSaving, userName);
			}

			@Override
			public void processIfResponseFail(String message) {
				// failed
				DialogManager.alert(self, "Login", message);
			}
		}, params);
		asyncLogin.execute(ApplicationClass.URL_LOGIN);
	}

	private void parseLogin(String response, boolean isSaving, String userName) {
		try {
			JSONObject _response = new JSONObject(response);
			String _errorCode = _response.getString("ERROR_CODE");
			String _message = _response.getString("MESSAGE");

			if ("0".equals(_errorCode)) {
				int _userID = Integer.parseInt(_response.getString("ID"));
				PreferencesManager _pref = PreferencesManager.getInstance(self);
				this.userName = userName;
				String _userToken = _response.getString("USERTOKEN");
				String _realName = _response.getString("REAL_NAME");
				String _email = _response.getString("EMAIL");
				String _avatar_href = _response.getString("AVATA_HREF");
				if (_realName != null && _realName.length() > 2) {
					ToastManager.showLongToastMessage(self, "Well come back "
							+ _realName);
				} else {
					ToastManager.showLongToastMessage(self, "Well come back "
							+ userName);
				}
				_pref.putStringValue(PreferencesManager.PREF_USER_NAME,
						userName);
				_pref.putStringValue(PreferencesManager.PREF_USER_TOKEN,
						_userToken);
				_pref.putBooleanValue(PreferencesManager.PREF_IS_SAVE_ACCOUNT,
						isSaving);
				_pref.putIntValue(PreferencesManager.PREF_USER_ID, _userID);
				_pref.putStringValue(PreferencesManager.PREF_USER_REAL_NAME,
						_realName);
				_pref.putStringValue(PreferencesManager.PREF_USER_EMAIL, _email);
				_pref.putStringValue(PreferencesManager.PREF_USER_AVARTAR_HREF,
						_avatar_href);
				// bt_login.setText("Logout");
				getDeviceToken();

			} else {
				DialogManager.alert(self, _message);
			}

		} catch (JSONException jsEx) {
			jsEx.printStackTrace();
		}
	}

	private void getDeviceToken() {

		boolean _isPlayServiceReady = checkPlayServices();
		if (_isPlayServiceReady) {
			gcm = GoogleCloudMessaging.getInstance(this);
			regid = getRegistrationId(self);
			if (regid.isEmpty()) {
				registerInBackground();
			} else {
				sendRegistrationIdToBackend(regid);
			}
		}
	}

}
