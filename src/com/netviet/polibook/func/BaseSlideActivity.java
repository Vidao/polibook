package com.netviet.polibook.func;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.netviet.polibook.R;
import com.slidingmenu.lib.app.SlideMenuActivity;

public class BaseSlideActivity extends SlideMenuActivity {
	
	
	public BaseSlideActivity self;
	public static boolean isActivityRunning = false;
	protected ProgressDialog progressDialog;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}
		self = this;
	}
	
	/**
     * Go to other activity
     *
     * @param context
     * @param cla
     */
    public void gotoActivity(Context context, Class<?> cla) {
        Intent intent = new Intent(context, cla);
        startActivity(intent);
    }
    /**
     * Go to other activity
     *
     * @param context
     * @param cla
     */
    public void gotoActivityWithIntentActionAndCategory(Context context, Class<?> cla, String intentAction, String category) {
    	Intent intent = new Intent(context, cla);
    	intent.setAction(intentAction);
    	intent.addCategory(category);
    	startActivity(intent);
    }
    /**
     * Go to other activity with flag
     *
     * @param context
     * @param cla
     * @param flag
     */
    public void gotoActivity(Context context, Class<?> cla, int flag) {
        Intent intent = new Intent(context, cla);
        intent.setFlags(flag);
        startActivity(intent);
    }
    /**
     * Go to other activity with request code
     *
     * @param context
     * @param cla
     */
    public void gotoActivityForResult(Context context, Class<?> cla,
                                      int requestCode) {
        Intent intent = new Intent(context, cla);
        startActivityForResult(intent, requestCode);
    }
    
    /**
     * Goto activity with bundle and request code
     *
     * @param context
     * @param cla
     * @param bundle
     * @param requestCode
     */
    public void gotoActivityForResult(Context context, Class<?> cla,
                                      Bundle bundle, int requestCode) {
        Intent intent = new Intent(context, cla);
        intent.putExtras(bundle);
        startActivityForResult(intent, requestCode);
    }

 // ======================= PROGRESS DIALOG ======================

    public void showProgressDialog(boolean isCancel) {
       
        showProgressDialog(getString(R.string.message_please_wait), isCancel);
    }

    /**
     * Open progress dialog
     *
     * @param text
     */
    public void showProgressDialog(String text) {
        if(progressDialog!= null && progressDialog.isShowing()){
            return;
        }
       
        if (progressDialog == null) {
            
            if (isActivityRunning && !isFinishing() ) {
                progressDialog = ProgressDialog.show(this,
                        getString(R.string.app_name), text, true);
                progressDialog.setCancelable(true);
                progressDialog.setCanceledOnTouchOutside(false);
            }
        }
    }

    public void showProgressDialog(String text, boolean isCancel) {
        if(progressDialog!= null && progressDialog.isShowing()){
            return;
        }
        if (progressDialog == null) {
            
            if (isActivityRunning) {
                progressDialog = ProgressDialog.show(this,
                        getString(R.string.app_name), text, true);
                progressDialog.setCancelable(isCancel);
                progressDialog.setCanceledOnTouchOutside(true);
            }
        }
    }

    
    /**
     * Open progress dialog
     *
     * @param textId
     */
    public void showProgressDialog(int textId) {
        if(progressDialog!= null && progressDialog.isShowing()){
            return;
        }
        if (progressDialog == null) {
            if (isActivityRunning) {
                progressDialog = ProgressDialog.show(this,
                        getString(R.string.app_name), getString(textId), true);
                progressDialog.setCancelable(true);
                progressDialog.setCanceledOnTouchOutside(true);
            }
        }
    }
    
    
    /**
     * Close progress dialog
     */
    public void closeProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing() && isActivityRunning) {
                progressDialog.dismiss();
            }
            progressDialog = null;
        }
    }

    
    protected void hideSoftKeyboard(int flags) {
        InputMethodManager inputManager = (InputMethodManager) self.getSystemService(Context.INPUT_METHOD_SERVICE);
        View view = self.getCurrentFocus();
        if (view != null) {
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), flags);
        }
    }
    
    

}
