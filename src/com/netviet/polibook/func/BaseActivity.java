/*
 * Name: $RCSfile: BaseActivity.java,v $
 * Version: $Revision: 1.1 $
 * Date: $Date: May 14, 2012 10:08:47 AM $
 *
 * Copyright (C) 2012 COMPANY_NAME, Inc. All rights reserved.
 */

package com.netviet.polibook.func;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gcm.GCMRegistrar;
import com.netviet.polibook.ApplicationClass;
import com.netviet.polibook.R;
import com.netviet.polibook.model.RingModel;
import com.netviet.polibook.utility.AudioManagement;
import com.netviet.polibook.utility.DialogManager;
import com.netviet.polibook.utility.FileUtil;
import com.netviet.polibook.utility.PreferencesManager;
import com.netviet.polibook.utility.ToastManager;

/**
 * BaseActivity class contains some base information and methods for other
 * activities
 * 
 * @author Hai Lee
 */
public class BaseActivity extends Activity {
	protected String TAG = "CLASS NAME";

	protected ApplicationClass app;

	protected BaseActivity self;

	protected ProgressDialog progressDialog, refreshDialog;

	protected PreferencesManager prefManager;

	// For logout, disconnect facebook
	protected boolean isLogoutApp = true;

	// GCM
	protected AsyncTask<Void, Void, Void> mRegisterTask;

	public static boolean isActivityRunning = false;

	/**
	 * Called when the activity is first created.
	 */

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onPause()
	 */
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	public AudioManagement audioManager;
	private Drawable noFadeThumbDrawable;
	protected Drawable thumbDrawable;
	protected void deleteCacheFile(ArrayList<RingModel> ringArray){
		for(RingModel _model: ringArray){
			String _filePath = _model.getLocalFilePath();
			if(_filePath!=null && _filePath.length()>5){
				File _file = new File(_filePath);
				if(_file.isFile()){
					FileUtil.delete(_file);
				}
				
			}
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}

		app = (ApplicationClass) getApplication();
		self = BaseActivity.this;
		prefManager = PreferencesManager.getInstance(getApplicationContext());
		TAG = this.getClass().getSimpleName();
		setTitle(getString(R.string.app_name));
		audioManager = new AudioManagement();
		noFadeThumbDrawable = getResources().getDrawable(
				R.drawable.seekbar_handle);
		thumbDrawable = getResources().getDrawable(R.drawable.seekbar_handle);
		initProgressBar();
	}

	/**
	 * Check invalid token
	 */
	public boolean isInvalidToken(boolean isInvalidToken) {
		if (isInvalidToken) {
			// Logout when invalid token
			DialogManager.sessionTimeout = true;
			logoutInvalidExpireToken();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Logout because of invalid token
	 */
	protected void logoutInvalidExpireToken() {
		ToastManager.showLongToastMessage(self,
				R.string.message_invalid_token_and_logout_message);
		logoutNormally();
	}

	/**
	 * Logout because of user logged in from other platform
	 */
	protected void logoutKickOut() {
	}

	/**
	 * Logout normally
	 */
	protected void logoutNormally() {

		// call API to remove device token on server
		if (!DialogManager.sessionTimeout) {
			removeDeviceToken();
		} else {
			// goto login activity here
			gotoActivity(self, MainActivity.class,
					Intent.FLAG_ACTIVITY_CLEAR_TOP);
		}
	}

	// ================== NAVIGATION METHOD ==================

	/**
	 * Go to other activity
	 * 
	 * @param context
	 * @param cla
	 */
	public void gotoActivity(Context context, Class<?> cla) {
		Intent intent = new Intent(context, cla);
		startActivity(intent);
	}

	public void gotoActivity(Context context, Class<?> cla, Bundle bundle) {
		Intent intent = new Intent(context, cla);
		intent.putExtras(bundle);
		startActivity(intent);
	}

	/**
	 * Go to other activity with flag
	 * 
	 * @param context
	 * @param cla
	 * @param flag
	 */
	public void gotoActivity(Context context, Class<?> cla, int flag) {
		Intent intent = new Intent(context, cla);
		intent.setFlags(flag);
		startActivity(intent);
	}

	/**
	 * Go to other activity with request code
	 * 
	 * @param context
	 * @param cla
	 */
	public void gotoActivityForResult(Context context, Class<?> cla,
			int requestCode) {
		Intent intent = new Intent(context, cla);
		startActivityForResult(intent, requestCode);
	}

	/**
	 * Goto activity with bundle and request code
	 * 
	 * @param context
	 * @param cla
	 * @param bundle
	 * @param requestCode
	 */
	public void gotoActivityForResult(Context context, Class<?> cla,
			Bundle bundle, int requestCode) {
		Intent intent = new Intent(context, cla);
		intent.putExtras(bundle);
		startActivityForResult(intent, requestCode);
	}

	/**
	 * Goto web page
	 * 
	 * @param url
	 */
	protected void gotoWebPage(String url) {
		Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		startActivity(i);
	}

	// ======================= PROGRESS DIALOG ======================

	public final Handler mHandleDialog = new Handler() {
		public void handleMessage(android.os.Message msg) {
			if (msg.what == 0) {
				showProgressDialog(false);
			}
		}

		;
	};

	/**
	 * Open progress dialog
	 * 
	 * @param text
	 */
	public void showProgressDialog(final String text, boolean isCancel) {

		if (progressDialog == null) {
			if (isActivityRunning) {
				progressDialog = ProgressDialog.show(self,
						getString(R.string.app_name), text, true);
				progressDialog.setCancelable(isCancel);
				progressDialog.setCanceledOnTouchOutside(isCancel);
			}
		}
	}
	
	
	

	/**
	 * Open progress dialog
	 * 
	 * @param textId
	 */
	public void showProgressDialog(int textId, boolean isCancel) {
		if (progressDialog == null) {
			if (isActivityRunning && !isFinishing()) {
				progressDialog = ProgressDialog.show(self,
						getString(R.string.app_name), getString(textId), true);
				progressDialog.setCancelable(isCancel);
				progressDialog.setCanceledOnTouchOutside(isCancel);
			}
		}
	}
	
	protected View pb_loading_more;
	protected void initProgressBar(){
		if(pb_loading_more == null){
			pb_loading_more = getLayoutInflater().inflate(R.layout.pb_layout, null);
			ProgressBar pb = (ProgressBar)pb_loading_more.findViewById(R.id.pb_loadmore);
			pb.setIndeterminate(true);
		}
	}
	
	
	

	/**
	 * Open progress dialog
	 */
	public void showProgressDialog(boolean isCancel) {
		if (DialogManager.sessionTimeout)
			return;
		showProgressDialog(getString(R.string.message_please_wait), isCancel);
	}

	/**
	 * Close progress dialog
	 */
	public void closeProgressDialog() {
		if (DialogManager.sessionTimeout)
			return;
		if (progressDialog != null) {
			if (progressDialog.isShowing() && isActivityRunning) {
				progressDialog.cancel();
			}
			progressDialog = null;
		}
	}

	/**
	 * Open refresh dialog
	 */
	public void showRefreshDialog() {
		if (refreshDialog == null) {
			if (isActivityRunning) {
				refreshDialog = ProgressDialog.show(self,
						getString(R.string.app_name),
						getString(R.string.message_refresh_inbox), true);
				refreshDialog.setCancelable(false);
			}
		}
	}

	/**
	 * Close refresh dialog
	 */
	public void closeRefreshDialog() {
		if (refreshDialog != null) {
			if (refreshDialog.isShowing() && isActivityRunning) {
				refreshDialog.cancel();
			}
			refreshDialog = null;
		}
	}

	// ========================= AdMod =========================

	private AdView adView;

	// =======================convert Pix and Dip===================

	/**
	 * px dip into
	 * 
	 * @param dipValue
	 * @return
	 */
	public int convertDips2Pixels(float dipValue) {
		return (int) convertDpToPixel(dipValue);
	}

	/**
	 * This method convets dp unit to equivalent device specific value in
	 * pixels.
	 * 
	 * @param dp
	 *            A value in dp(Device independent pixels) unit. Which we need
	 *            to convert into pixels
	 * @param context
	 *            Context to get resources and device specific display metrics
	 * @return A float value to represent Pixels equivalent to dp according to
	 *         device
	 */
	public float convertDpToPixel(float dipValue) {
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		float px = dipValue * (metrics.densityDpi / 160f);
		return px;
	}

	/**
	 * This method converts device specific pixels to device independent pixels.
	 * 
	 * @param px
	 *            A value in px (pixels) unit. Which we need to convert into db
	 * @param context
	 *            Context to get resources and device specific display metrics
	 * @return A float value to represent db equivalent to px value
	 */
	public float convertPixelsToDp(float pixels) {
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		float dp = pixels / (metrics.densityDpi / 160f);
		return dp;
	}

	// ====================GCM===================================

	protected void hideSoftKeyboard(int flags) {
		InputMethodManager inputManager = (InputMethodManager) self
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		View view = self.getCurrentFocus();
		if (view != null) {
			inputManager.hideSoftInputFromWindow(view.getWindowToken(), flags);
		}
	}

	

	private SeekBar currentSeekBar;
	private ImageView currentIv_play;
	private LinearLayout currentLoutRingInfo;
	private RingModel currentRingModel;
	private LinearLayout currentPictureLayout;

	public RingModel getCurrentRingModel() {
		return currentRingModel;
	}

	public void setCurrentRingModel(RingModel currentRingModel) {
		this.currentRingModel = currentRingModel;
	}

	public LinearLayout getCurrentPictureLayout() {
		return currentPictureLayout;
	}

	public void setCurrentPictureLayout(LinearLayout currentPictureLayout) {
		this.currentPictureLayout = currentPictureLayout;
	}

	public SeekBar getCurrentProgressBar() {
		return currentSeekBar;
	}

	public void setCurrentProgressBar(SeekBar currentSeekBar) {
		this.currentSeekBar = currentSeekBar;
	}

	protected void seekBarProgressUpdater() {
		try {
			// Log.d( "handler update progress : " + playIndex);
			
			if (audioManager.isPlaying()) {
				// Get playing progress
				// int progressNewInt = player.getCurrentPosition();

				if (audioManager.player_current < audioManager.player_total) {
					int curPos = audioManager.getCurrentPlayingState();

					// Update playing status

					if (audioManager.player_current <= 0
							|| currentSeekBar.getMax() == 100) {
						currentSeekBar.setMax(audioManager.player_total);
					}
					// if (curPos < audioManager.player_current) {
					// audioManager.player_current = audioManager.player_total;
					// } else {
					audioManager.player_current = curPos;
					// }
					
					currentSeekBar.setProgress(audioManager.player_current);
					noFadeThumbDrawable.setAlpha(255);

					currentSeekBar.setThumb(noFadeThumbDrawable);
					currentSeekBar.setThumbOffset(1);
					currentSeekBar.getProgressDrawable().setAlpha(255);

//					setBabbleRowToPlayingMode(true);
					// setBabbleRowPictureMode(babbleRow,
					// totalBabbleList.get(playIndex), true);
					// Update UI
					Runnable notification = new Runnable() {
						@Override
						public void run() {
							seekBarProgressUpdater();
						}
					};
					handler.postDelayed(notification, 100);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	protected void setBabbleRowToPlayingMode(boolean isPlayingMode) {
		if(currentRingModel == null || currentIv_play == null){
			return;
		}
		String _pictureHref = currentRingModel.getImageHref();
		
		if (isPlayingMode) {
			currentIv_play.setBackgroundResource(R.drawable.btn_play_activated);
			currentLoutRingInfo.setVisibility(View.GONE);
			currentSeekBar.setVisibility(View.VISIBLE);
			if (_pictureHref != null && _pictureHref.length() > 5
					&& currentPictureLayout != null) {
				currentPictureLayout.setVisibility(View.VISIBLE);
			} else {
				currentPictureLayout.setVisibility(View.GONE);
			}
		} else {
			currentIv_play.setBackgroundResource(R.drawable.btn_play);
			currentLoutRingInfo.setVisibility(View.VISIBLE);
			currentSeekBar.setVisibility(View.GONE);

			if (_pictureHref != null && _pictureHref.length() > 5
					&& currentPictureLayout != null) {
				currentPictureLayout.setVisibility(View.GONE);
			}
		}

	}

	public ImageView getCurrentIv_play() {
		return currentIv_play;
	}

	public void setCurrentIv_play(ImageView currentIv_play) {
		this.currentIv_play = currentIv_play;
	}

	public LinearLayout getCurrentLoutRingInfo() {
		return currentLoutRingInfo;
	}

	public void setCurrentLoutRingInfo(LinearLayout currentLoutRingInfo) {
		this.currentLoutRingInfo = currentLoutRingInfo;
	}

	private final Handler handler = new Handler();
	protected final OnSeekBarChangeListener onSeekBarChange = new OnSeekBarChangeListener() {
		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
			int seek2Pos = seekBar.getProgress();
			try {
				AudioManagement.playingStatus = AudioManagement.PLAYING_BABBLE_ROW;
				audioManager.player_current = seek2Pos;
				audioManager.player.pause();
				// Thread.sleep(50)
				audioManager.seekTo(seek2Pos);
				audioManager.player.start();

				seekBarProgressUpdater();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {
			try {
				audioManager.player.pause();
			} catch (Exception e) {
			}
		}

		@Override
		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			// no use
		}
	};

	/**
	 * updateDeviceToken
	 * 
	 * @param registrationId
	 * @param deviceType
	 *            1: iphone, 2: android
	 */
	protected void updateDeviceToken(String registrationId, int deviceType) {
		// UserInfo user = PreferencesManager.getInstance(self).getUserInfo();
		// List<NameValuePair> params =
		// ParameterFactory.createUpdateDeviceTokenParams(
		// user, registrationId, deviceType);
		// MyAsyncHttpPost updateTokenPost = new MyAsyncHttpPost(self,
		// new MyAsyncHttpResponseProcess(self) {
		// @Override
		// public void processIfResponseSuccess(String response) {
		// GCMRegistrar.setRegisteredOnServer(self, true);
		// }
		//
		// @Override
		// public void processIfResponseFail(String message) {
		// DialogManager.alert(
		// self,
		// message
		// +
		// " You will not able to receive notifications. Please check internet connection and restart application.");
		// GCMRegistrar.unregister(self);
		// }
		// }, params);
		// updateTokenPost.execute(WebServiceConfig.URL_NOTIFICATION);
	}

	/**
	 * notify server to remove DeviceToken
	 */
	protected void removeDeviceToken() {

		// UserInfo user = PreferencesManager.getInstance(self).getUserInfo();
		// List<NameValuePair> params =
		// ParameterFactory.createRemoveDeviceTokenParams(user);
		// MyAsyncHttpPost removeTokenPost = new MyAsyncHttpPost(self,
		// new MyAsyncHttpResponseProcess(self) {
		// @Override
		// public void processIfResponseSuccess(String response) {
		// GCMRegistrar.setRegisteredOnServer(self, false);
		// GCMRegistrar.unregister(self);
		// prefManager.clearUserInfo();
		// prefManager.showUserInfo();
		// gotoActivity(self, SignInActivity.class,
		// Intent.FLAG_ACTIVITY_CLEAR_TOP);
		// // GCMRegistrar.onDestroy(self);
		// }
		//
		// @Override
		// public void processIfResponseFail(String message) {
		// prefManager.clearUserInfo();
		// prefManager.showUserInfo();
		// gotoActivity(self, SignInActivity.class,
		// Intent.FLAG_ACTIVITY_CLEAR_TOP);
		// }
		//
		// }, params);
		// removeTokenPost.execute(WebServiceConfig.URL_ACTION_BABBLE);
	}

	public interface HandleDialog {
		public void showDialog();

		public void dismissDialog();
	}

}
