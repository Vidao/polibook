/*
 * Name: $RCSfile: ToastManager.java,v $
 * Version: $Revision: 1.1 $
 * Date: $Date: May 14, 2012 4:27:09 PM $
 *
 * Copyright (C) 2012 COMPANY_NAME, Inc. All rights reserved.
 */

package com.netviet.polibook.utility;

import com.netviet.polibook.R;

import android.content.Context;
import android.widget.Toast;


/**
 * ToastManager enables show toast message
 *
 * @author Hai Lee
 */
public final class ToastManager {
    /**
     * Show long toast message
     *
     * @param context
     * @param str
     */
    public static void showLongToastMessage(Context context, String str) {
        Toast.makeText(context, str, Toast.LENGTH_LONG).show();
    }

    /**
     * Show long toast message
     *
     * @param context
     * @param strId
     */
    public static void showLongToastMessage(Context context, int strId) {
        Toast.makeText(context, context.getString(strId), Toast.LENGTH_LONG).show();
    }

    /**
     * Show short toast message
     *
     * @param context
     * @param str
     */
    public static void showShortToastMessage(Context context, String str) {
        Toast.makeText(context, str, Toast.LENGTH_SHORT).show();
    }

    /**
     * Show long toast message
     *
     * @param context
     * @param strId
     */
    public static void showShortToastMessage(Context context, int strId) {
        Toast.makeText(context, context.getString(strId), Toast.LENGTH_SHORT).show();
    }

//    /**
//     * Show comming soon toast message
//     */
//    public static void showCommingSoonMessage(Context context) {
//        showLongToastMessage(context,
//                context.getString(R.string.text_coming_soon));
//    }
}
