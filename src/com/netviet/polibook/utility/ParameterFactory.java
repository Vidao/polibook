package com.netviet.polibook.utility;

import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.netviet.polibook.ApplicationClass;

public final class ParameterFactory {

	public static List<NameValuePair> addParticipantParam(String userName,
			String token, String groupID, String listMember) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair(ApplicationClass.PARAM_INPUT_USER_NAME,
				userName));
		parameters.add(new BasicNameValuePair(
				ApplicationClass.PARAM_INPUT_USER_TOKEN, token));
		parameters.add(new BasicNameValuePair(ApplicationClass.PARAM_INPUT_GROUP_ID,
				groupID));
		parameters.add(new BasicNameValuePair(
				ApplicationClass.PARAM_INPUT_USER_MEMBER_ID, listMember));
		return parameters;
	}

	public static List<NameValuePair> deviceTokenParam(String userName,
			String token, String deviceToken) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();

		parameters.add(new BasicNameValuePair(ApplicationClass.PARAM_INPUT_USER_NAME,
				userName));

		parameters.add(new BasicNameValuePair(
				ApplicationClass.PARAM_INPUT_USER_TOKEN, token));

		parameters.add(new BasicNameValuePair(
				ApplicationClass.PARAM_INPUT_DEVICE_TOKEN, deviceToken));
		parameters.add(new BasicNameValuePair(
				ApplicationClass.PARAM_INPUT_DEVICE_TOKEN_ID, "0"));

		return parameters;
	}
	

	public static List<NameValuePair> signUpParam(String userName,
			String passWord, String phoneNumber) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();

		parameters.add(new BasicNameValuePair(ApplicationClass.PARAM_INPUT_USER_NAME,
				userName));

		parameters.add(new BasicNameValuePair(ApplicationClass.PARAM_INPUT_PASSWORD,
				passWord));

		parameters.add(new BasicNameValuePair(ApplicationClass.PARAM_INPUT_PHONE,
				phoneNumber));

		return parameters;
	}
	
	
	public static List<NameValuePair> loginParam(String userName,
			String passWord) {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();

		parameters.add(new BasicNameValuePair(ApplicationClass.PARAM_INPUT_USER_NAME,
				userName));

		parameters.add(new BasicNameValuePair(ApplicationClass.PARAM_INPUT_PASSWORD,
				passWord));

		parameters.add(new BasicNameValuePair(
				ApplicationClass.PARAM_INPUT_DEVICE_TOKEN_ID, "0"));

		return parameters;
	}

}
