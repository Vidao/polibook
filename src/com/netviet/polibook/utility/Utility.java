/**
 *
 */
package com.netviet.polibook.utility;

import java.io.File;
import java.util.Locale;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.ImageView;

/**
 * @author vidp
 */
public class Utility {

	/**
	 * Get current time in milliseconds
	 * 
	 * @return
	 */
	public static long now() {
		return System.currentTimeMillis();
	}

	/**
	 * Generate file name automatically based on current time stamp
	 * 
	 * @return
	 */
	public static String currentTimeString() {
		return String.format(Locale.US, "%d", System.currentTimeMillis());
	}

	/**
	 * Merge all elements of a string array into a string
	 * 
	 * @param strings
	 * @param separator
	 * @return
	 */
	public static String join(String[] strings, String separator) {
		StringBuffer sb = new StringBuffer();
		int max = strings.length;
		for (int i = 0; i < max; i++) {
			if (i != 0)
				sb.append(separator);
			sb.append(strings[i]);
		}
		return sb.toString();
	}

	/**
	 * Merge all of a string array into a string
	 * 
	 * @param strings
	 * @param separator
	 * @param start
	 * @param end
	 * @return
	 */
	public static String join(String[] strings, String separator, int start,
			int end) {
		StringBuffer sb = new StringBuffer();
		for (int i = start; i < end; i++) {
			if (i != start)
				sb.append(separator);
			sb.append(strings[i]);
		}
		return sb.toString();
	}

	/**
	 * Read bit map data from image file and feed it to ImageView
	 * 
	 * @param imgView
	 * @param file
	 */
	public static void loadImageView(ImageView imgView, File file) {
		Bitmap bm = BitmapFactory.decodeFile(file.getAbsolutePath());
		imgView.setImageBitmap(bm);
	}

	/**
	 * px dip into
	 * 
	 * @param dipValue
	 * @return
	 */
	public static int convertDips2Pixels(float dipValue, Context context) {
		return (int) convertDpToPixel(dipValue, context);
	}

	/**
	 * This method convets dp unit to equivalent device specific value in
	 * pixels.
	 * 
	 * @param dp
	 *            A value in dp(Device independent pixels) unit. Which we need
	 *            to convert into pixels
	 * @param context
	 *            Context to get resources and device specific display metrics
	 * @return A float value to represent Pixels equivalent to dp according to
	 *         device
	 */
	public static float convertDpToPixel(float dipValue, Context context) {
		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		float px = dipValue * (metrics.densityDpi / 160f);
		return px;
	}

	/**
	 * This method converts device specific pixels to device independent pixels.
	 * 
	 * @param px
	 *            A value in px (pixels) unit. Which we need to convert into db
	 * @param context
	 *            Context to get resources and device specific display metrics
	 * @return A float value to represent db equivalent to px value
	 */
	public static float convertPixelsToDp(float pixels, Context context) {
		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		float dp = pixels / (metrics.densityDpi / 160f);
		return dp;
	}

	public static Bitmap scaleImage(Bitmap bitmap, int newHeight,
			Context context) {
		// Get current dimensions AND the desired bounding box
		int width = bitmap.getWidth();
		int height = bitmap.getHeight();
		int newWidth = (int) (((float) newHeight / (float) height) * width);

		// Determine how much to scale: the dimension requiring less scaling
		// is
		// closer to the its side. This way the image always stays inside
		// your
		// bounding box AND either x/y axis touches it.
		float xScale = ((float) Utility.convertDips2Pixels(newWidth, context))
				/ width;
		float yScale = ((float) Utility.convertDips2Pixels(newHeight, context))
				/ height;
		float scale = (xScale <= yScale) ? xScale : yScale;

		// Create a matrix for the scaling and add the scaling data
		Matrix matrix = new Matrix();
		matrix.postScale(scale, scale);

		// Create a new bitmap and convert it to a format understood by the
		// ImageView
		Bitmap scaledBitmap = Bitmap.createBitmap(bitmap, 0, 0, newWidth,
				newHeight, matrix, true);
		width = scaledBitmap.getWidth(); // re-use
		height = scaledBitmap.getHeight(); // re-use
		return scaledBitmap;
	}

	public static ImageLoaderConfiguration imageLoaderConfig;

	public static void setImageConfig(Context applicationContext, int width) {
		File cacheDir = StorageUtils.getOwnCacheDirectory(applicationContext,
				"com/babblesphere/UniversalImageLoader/Cache");
		imageLoaderConfig = new ImageLoaderConfiguration.Builder(
				applicationContext).threadPoolSize(3)
				.threadPriority(Thread.MAX_PRIORITY)
				.memoryCache(new UsingFreqLimitedMemoryCache(2000000))
				.denyCacheImageMultipleSizesInMemory()
				.discCacheExtraOptions(480, 800, CompressFormat.JPEG, 75, null)
				.build();

		// imageLoaderConfig = new ImageLoaderConfiguration.Builder(
		// applicationContext).threadPoolSize(3).threadPriority(
		// Thread.NORM_PRIORITY).memoryCache(
		// new
		// UsingFreqLimitedMemoryCache(2000000)).memoryCacheSize(1500000).denyCacheImageMultipleSizesInMemory()
		// .discCacheExtraOptions(
		// 800, 800, CompressFormat.PNG, 0)
		// .imageDownloader(
		// new BaseImageDownloader(applicationContext)).discCache(
		// new UnlimitedDiscCache(cacheDir)).discCacheFileNameGenerator(
		// new HashCodeFileNameGenerator()).build();

		// imageLoaderConfig = new ImageLoaderConfiguration.Builder(
		// applicationContext).memoryCacheExtraOptions(width,
		// width).discCacheExtraOptions(
		// width, width, CompressFormat.JPEG, 75).threadPoolSize(2) // default
		// .threadPriority(Thread.NORM_PRIORITY - 2) // default
		// .denyCacheImageMultipleSizesInMemory()
		// .memoryCache(new UsingFreqLimitedMemoryCache(2 * 300 * 300)) //
		// default
		// .memoryCacheSize(2 * 512 * 512).discCache(
		// new UnlimitedDiscCache(cacheDir)) // default
		// .discCacheSize(50 * 768 *
		// 768).discCacheFileCount(100).discCacheFileNameGenerator(
		// new HashCodeFileNameGenerator()) // default
		// .imageDownloader(new BaseImageDownloader(applicationContext)) //
		// default
		// .defaultDisplayImageOptions(DisplayImageOptions.createSimple()) //
		// default
		// .build();
	}

}
