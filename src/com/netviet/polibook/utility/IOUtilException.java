package com.netviet.polibook.utility;

public class IOUtilException extends Exception {
	private static final long serialVersionUID = -2114608377153323594L;
	public IOUtilException(String msg) {
		super(msg);
	}
}
