/*
 * Name: $RCSfile: AudioManagement.java,v $
 * Version: $Revision: 1.1 $
 * Date: $Date: Jun 10, 2013 5:54:43 PM $
 *
 * Copyright (C) 2012 COMPANY NAME, Inc. All rights reserved.
 */
package com.netviet.polibook.utility;

import java.io.FileDescriptor;
import java.io.IOException;

import com.netviet.polibook.GCMIntentService;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;

/**
 * @author User
 * 
 */
public class AudioManagement implements OnBufferingUpdateListener {

	public static int mode;
	public static final int MODE_DOWNLOADING = 1;
	public static final int MODE_READY_TO_PLAY = 2;
	public static final int MODE_PLAYING = 3;

	public static final int PLAYING_BABBLE_ROW = 0;
	public static final int PLAYING_RECORD = 1;
	public static final int PLAYING_PLAYER = 2;
	public static final int PLAYING_AUTO = 3;
	public static boolean isWaitForAutoPlay = false;
	// to detect play start record sound or play stop record sound
	public static boolean isStartRecord = true;
	public static boolean isPlaySound = false;
	public Context context = null;

	public static int playingStatus = PLAYING_BABBLE_ROW;
	public static boolean isLoading = false;
	public int player_current = 0;
	public int player_total = 0;
	public int playerTotal = 0;
	public int playerCurrent = 0;
	public static boolean isAutoPlay = false;
	public static boolean isPlaying = false;
	private int ringState = MODE_READY_TO_PLAY;

	public int getRingState() {
		return ringState;
	}

	// MediaPlayer.OnVideoSizeChangedListener mOnVideoSizeChangedListener = new
	// MediaPlayer.OnVideoSizeChangedListener() {
	// public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
	//
	// }
	// };
	public void setRingState(int ringState) {
		this.ringState = ringState;
	}

	public JMediaPlayer player = JMediaPlayer.getJMediaPlayer();

	public void stopPlayInAuto() {
		if (playingStatus == PLAYING_AUTO) {

			isAutoPlay = false;
			isPlaying = false;
			stopPlay(null);
			clearAutoPlayQueue();
			PlayerAutoInBackground.isLoading = false;

		}
	}

	public void stopPlay(Context self) {
		// if (!isPlaying) {
		// return;
		// }
		isPlaying = false;

		if (isPlaySound && !isStartRecord) {
			isPlaySound = false;
		}

		if (player != null) {
			player.stop();
			player.reset();
			// player.release();
			player_current = 0;
			player_total = 0;
			playerTotal = 0;
			playerCurrent = 0;
		}
		if (!isPlaySound && isWaitForAutoPlay) {
			startPlayInAuto(self);
		}
	}

	public synchronized void startPlay(String filepath)
			throws IllegalArgumentException, IllegalStateException, IOException {

		isPlaying = true;
		if (player.isNewSession()) {
			// Set player for recored babble
			startPlayerHandler(filepath);
		}
	}

	public int getDurationPlayer() {
		if (player != null) {
			return player.getDuration();
		} else {
			return 0;
		}
	}

	public boolean isPlaying() {
		if (player != null) {
			return player.isPlaying();
		} else {
			return false;
		}
	}

	public static void clearAutoPlayQueue() {
		GCMIntentService.notification_queue.removeAllElements();
	}

	public void startPlayerHandler(String filePath)
			throws IllegalArgumentException, IllegalStateException, IOException {

		if (filePath == null || filePath.length() < 5) {
			PlayerAutoInBackground.isLoading = false;
			return;
		}
		isPlaying = true;
		player.setOnBufferingUpdateListener(this);
		if (filePath.indexOf("http") != -1) {
			System.out.println("path:: "+filePath);
			player.setAudioStreamType(AudioManager.STREAM_MUSIC);
			Uri myUri = Uri.parse(filePath);
			player.setDataSource(context,myUri);
		}else{
			player.setDataSource(filePath);
		}
		
		player.setOnPreparedListener(new OnPreparedListener() {
			@Override
			public void onPrepared(MediaPlayer mp) {
			}
		});
		player.prepare();
		player.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
			public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
			}
		});
		// Update seek bar
		if (playingStatus == PLAYING_PLAYER) {
			playerTotal = player.getDuration();
			playerCurrent = 0;
		}
		player_current = 0;
		player_total = player.getDuration();
		// Start play recored babble
		// playingStatus = PLAYING_BABBLE_ROW;
		player.start();
	}

	public void startPlayerHandler(FileDescriptor filePath, long offset,
			long length) throws IllegalArgumentException,
			IllegalStateException, IOException {

		isPlaying = true;
		isPlaySound = true;
		player.setDataSource(filePath, offset, length);
		player.prepare();
		// Update seek bar
		playerCurrent = 0;
		playerTotal = 0;
		player_current = 0;
		player_total = player.getDuration();
		// Start play recored babble
		// playingStatus = PLAYING_BABBLE_ROW;
		player.start();
	}

	public void seekTo(int position) {
		if (player != null) {
			player.seekTo(position);
		}
	}

	public static void startPlayInAuto(Context context) {
		isPlaying = false;
		PlayerAutoInBackground _autoPlay = new PlayerAutoInBackground(context);
		isWaitForAutoPlay = false;
		_autoPlay.start();
	}

	public int getCurrentPlayingState() {
		return player.getCurrentPosition();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.media.MediaPlayer.OnBufferingUpdateListener#onBufferingUpdate
	 * (android.media.MediaPlayer, int)
	 */
	@Override
	public void onBufferingUpdate(MediaPlayer mp, int percent) {
		// TODO Auto-generated method stub

	}

}
