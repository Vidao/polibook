package com.netviet.polibook.utility;

public interface ErrorTracker {

    void exceptionThrownFromThread(Thread thread, Exception e);

    void message(String msg);
}
