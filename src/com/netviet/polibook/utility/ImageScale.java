/*
 * Name: $RCSfile: ImageScale.java,v $
 * Version: $Revision: 1.1 $
 * Date: $Date: Feb 28, 2013 10:51:19 AM $
 *
 * Copyright (C) 2012 COMPANY NAME, Inc. All rights reserved.
 */
package com.netviet.polibook.utility;

//import android.app.ActionBar.LayoutParams;
import android.graphics.LightingColorFilter;
//import android.renderscript.Type;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

/**
 * @author vidp
 */
public class ImageScale {
	ImageView imgInput;

	/**
	 * @return the imgInput
	 */
	public ImageView getImgInput() {
		return imgInput;
	}

	/**
	 * @param imgInput
	 *            the imgInput to set
	 */
	public void setImgInput(ImageView imgInput) {
		this.imgInput = imgInput;
	}

	int duration;
	int scaleType;
	int scaleNumber;
	public static final int SCALE_TO_PIXEL = 0;
	public static final int SCALE_TO_DIP = 1;
	public boolean isShowing = false;

	public ImageScale(int duration, int scaleType, int scaleNumber) {
		this.duration = duration;
		this.scaleType = scaleType;
		this.scaleNumber = scaleNumber;
	}

	public void scaleImage() {
		switch (scaleType) {
		case SCALE_TO_PIXEL:
			// process scale to pixel here
			scaleToPix();
			break;

		case SCALE_TO_DIP:
			// process scale to dip here

			break;
		default:
			break;
		}
	}

	private void scaleToPix() {

		if (!isShowing) {
			float scaleDif = (scaleNumber) / (imgInput.getWidth()) + 0.5f;
			ScaleAnimation animation = new ScaleAnimation(1.0f, scaleDif, 1.0f,
					scaleDif);

			disScaleDif = (1.0f / (scaleDif - 0.5f));
			animation.setAnimationListener(new Animation.AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub
				}

				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub
				}

				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub
					LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
							scaleNumber, scaleNumber + 40);
					imgInput.setLayoutParams(param);
					imgInput.clearAnimation();
				}
			});
			animation.setDuration(500);
			animation.setFillAfter(true);
			imgInput.setAnimation(animation);
			LinearLayout _layoutImage = (LinearLayout) imgInput.getParent();

			currentHeigh = _layoutImage.getHeight();
			currentImageHeight = imgInput.getHeight();
			currentImageWidth = imgInput.getWidth();
			if (_layoutImage.getLayoutParams().getClass()
					.equals(LinearLayout.LayoutParams.class)) {
				_layoutImage.setLayoutParams(new LinearLayout.LayoutParams(
						scaleNumber, scaleNumber + 40));
			} else if (_layoutImage.getLayoutParams().getClass()
					.equals(RelativeLayout.LayoutParams.class)) {
				_layoutImage.setLayoutParams(new RelativeLayout.LayoutParams(
						scaleNumber, scaleNumber + 40));
			} else if (_layoutImage.getLayoutParams().getClass()
					.equals(AbsListView.LayoutParams.class)) {
				_layoutImage.setLayoutParams(new AbsListView.LayoutParams(
						scaleNumber, scaleNumber + 40));
			}
			imgInput.setPadding(2, 10, 0, 0);
			imgInput.startAnimation(animation);
			isShowing = true;
		} else {
			System.out.println("run in else:::::::::::::::::::;");
			ScaleAnimation animation = new ScaleAnimation(1.0f, disScaleDif,
					1.0f, disScaleDif);
			animation.setAnimationListener(new Animation.AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub
				}

				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub
				}

				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub
					LinearLayout _layoutImage = (LinearLayout) imgInput
							.getParent();
					if (_layoutImage.getLayoutParams().getClass()
							.equals(LinearLayout.LayoutParams.class)) {
						_layoutImage
								.setLayoutParams(new LinearLayout.LayoutParams(
										LinearLayout.LayoutParams.FILL_PARENT,
										currentHeigh));
					} else if (_layoutImage.getLayoutParams().getClass()
							.equals(RelativeLayout.LayoutParams.class)) {
						_layoutImage
								.setLayoutParams(new RelativeLayout.LayoutParams(
										RelativeLayout.LayoutParams.FILL_PARENT,
										currentHeigh));
					} else if (_layoutImage.getLayoutParams().getClass()
							.equals(AbsListView.LayoutParams.class)) {
						_layoutImage
								.setLayoutParams(new AbsListView.LayoutParams(
										AbsListView.LayoutParams.FILL_PARENT,
										currentHeigh));
					}
					_layoutImage.setMinimumHeight(currentHeigh);
					imgInput.setPadding(-2, -10, 0, 0);
					LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
							currentImageWidth, currentImageHeight);
					param.setMargins(0, 2, 0, 0);
					imgInput.setLayoutParams(param);
					imgInput.clearAnimation();
				}
			});
			animation.setDuration(500);
			animation.setFillAfter(true);
			imgInput.setAnimation(animation);
			imgInput.startAnimation(animation);
			isShowing = false;
		}
	}

	int currentImageWidth;
	int currentImageHeight;
	int currentHeigh;
	float disScaleDif;

}
