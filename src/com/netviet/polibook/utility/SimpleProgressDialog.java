package com.netviet.polibook.utility;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * @author Administrator
 */
public class SimpleProgressDialog {

	private ProgressDialog dialog;

	private static SimpleProgressDialog inst = null;

	private SimpleProgressDialog() {
	}

	/**
	 * Create handler for progress dialog
	 * 
	 * @return
	 */
	public static SimpleProgressDialog instance() {
		if (inst == null) {
			inst = new SimpleProgressDialog();
		}
		return inst;
	}

	/**
	 * Create and show progress dialog
	 * 
	 * @param context
	 * @param message
	 * @param flag
	 * @return
	 */
	public boolean open(Context context, String message, boolean flag) {
		if (dialog == null) {
			// boolean isActivityRunning = false;
			dialog = new ProgressDialog(context);
			dialog.setCancelable(flag);
			dialog.setMessage(message);

			dialog.show();

			return true;
		} else {
			return false;
		}
	}

	/**
	 * Close the progress dialog
	 */
	public void close() {
		if (dialog != null) {
			dialog.dismiss();
			dialog = null;
		}
	}

}
