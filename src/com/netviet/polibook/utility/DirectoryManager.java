/**
 *
 */
package com.netviet.polibook.utility;

import java.io.File;

import com.netviet.polibook.PacketUtility;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

/**
 * @author cuongvm6037
 */
public class DirectoryManager {
    protected static final String TAG = "DirectoryUtil";

    /**
     * Open a directory in external storage (such as SD card)
     *
     * @param dirName
     * @param createIfNotExist
     * @return
     * @throws IOUtilException
     */
    public static File openExternalDir(String dirName, boolean createIfNotExist)
            throws IOUtilException {
        try {
            File exStorageDir = Environment.getExternalStorageDirectory();
            File dir = new File(exStorageDir.getAbsolutePath() + "/" + dirName);
            if (createIfNotExist) {
                dir.mkdirs();
            }
            return dir;
        } catch (Exception e) {
            throw new IOUtilException("Can not create directory. Please check your SD card.");
        }
    }

//    /**
//     * Open user project directory in SD card
//     *
//     * @param context
//     * @return
//     * @throws IOUtilException
//     */
//    public static File openUserDirectory(Context context)
//            throws IOUtilException {
//        String userId = "1214";
//        return DirectoryManager.openExternalDir(
//                PacketUtility.getInstance().getPacketName(userId), true);
//    }

    /**
     * Get application folder path in SD Card
     *
     * @param dirName
     * @param createIfNotExist
     * @return
     */
    public static String getExternalDir(String dirName, boolean createIfNotExist) {
        String path = "";
        if (MemoryUtility.hasExternalStorage()) {
            try {
                File exStorageDir = Environment.getExternalStorageDirectory();
                File dir = new File(exStorageDir.getAbsolutePath() + "/"
                        + dirName);
                if (createIfNotExist) {
                    dir.mkdirs();
                }
                path = dir.getAbsolutePath();
            } catch (Exception e) {
                path = "";
            }
        }
        return path;
    }

    /**
     * Get application folder path in SD Card
     *
     * @param dirName
     * @param createIfNotExist
     * @return
     */
    public static String getAppSdCardFolder() {
        return getExternalDir(PacketUtility.getInstance().getPacketName(), true);
    }

    /**
     * Get user folder path in SD Card
     *
     * @param context
     * @return
     */
    public static String getUserSdCardFolder(Context context) {
        String userId = "1214";
        return getExternalDir(
        PacketUtility.getInstance().getPacketName(userId), true);
    }

    /**
     * Open a directory within application data folder. If the directory does
     * not exist, this method will create it before returning File handle to the
     * directory. If the directory exists, this method simply return File handle
     * of the directory.
     *
     * @param dirName
     * @param packageName
     * @param createIfNotExist
     * @return
     * @throws IOUtilException
     */
    public static File openDataDir(String dirName, String packageName,
                                   boolean createIfNotExist) throws IOUtilException {
        try {
            String path = "/data/data/" + packageName + "/databases/" + dirName;
            File dir = new File(path);
            if (createIfNotExist) {
                dir.mkdirs();
            }
            return dir;
        } catch (Exception e) {
            throw new IOUtilException("Can not create directory");
        }
    }
}
