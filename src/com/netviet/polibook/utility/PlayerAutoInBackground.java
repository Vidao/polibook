/*
 * Name: $RCSfile: PlayerAutoInBackground.java,v $
 * Version: $Revision: 1.1 $
 * Date: $Date: Jun 11, 2013 2:26:56 PM $
 *
 * Copyright (C) 2012 COMPANY NAME, Inc. All rights reserved.
 */
package com.netviet.polibook.utility;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.netviet.polibook.GCMIntentService;
import com.netviet.polibook.R;
import com.netviet.polibook.net.MyAsyncBabble;
import com.netviet.polibook.net.MyAsyncBabbleResponseProcess;

import android.content.Context;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.view.View;


/**
 * @author User
 * 
 */
public class PlayerAutoInBackground extends Thread implements
        JMediaPlayer.OnCompletionListener, OnBufferingUpdateListener
{

    String URLPath;
    String notificationID;

    private String physical_name;
    private Context context;
    public static final String FOLLOWING_PHYSICAL_NAME = "temp_physical_following.mp3";
    public static final String GROUP_PHYSICAL_NAME = "temp_physical_group.mp3";
    public static final String MESSAGE_PHYSICAL_NAME = "temp_physical_message.mp3";
    public static final String NOTIFICATION_UPDATE_URL = "temp_physical_message.mp3";
    public static boolean isLoading = false;
    private AudioManagement mediaManager;
    String content[] = new String[2];
    MyAsyncBabble asyncDownloadBabble = null;

    public PlayerAutoInBackground(Context context)
    {
        String _content = "";
//        if (GCMIntentService.notification_queue.size() > 0)
//        {
//            _content = GCMIntentService.notification_queue.pop();
//        }
//        content = _content.split(PreferencesManager.QUOTE_HISTORY_ITEM_SPLIT);
//        if (content != null && content.length >= 2)
//
//        {
//            this.URLPath = content[0];
//            this.notificationID = content[1];
//            this.context = context;
//            mediaManager = new AudioManagement();
//            mediaManager.player.setOnCompletionListener(this);
//        }
    }

    @Override
    public void run()
    {
        // TODO Auto-generated method stub
        if (mediaManager != null && !mediaManager.isPlaying()
            && URLPath != null && URLPath.length() > 0)
        {
            // save to history;
            // saveToHistory();
            // play babble
            String physicalBabbleName = "autoplaytemp";
            getBabbleFromServer(physicalBabbleName, URLPath);
            // updateNotification(notificationID);
            updateReadBabble(notificationID);

        }
    }

    /**
     * @param notificationID2
     */
    private void updateReadBabble(String notificationID)
    {

//        UserInfo userInfo = PreferencesManager.getInstance(context).getUserInfo();
//        try
//        {
//            HttpClient httpClient = new DefaultHttpClient();
//            String _URL = WebServiceConfig.URL_NOTIFICATION;
//            HttpPost httpPost = new HttpPost(_URL);
//            List<NameValuePair> parameters = ParameterFactory.createUpdateAutoPlayNotificationParams(
//                userInfo, notificationID);
//            httpPost.setEntity(new UrlEncodedFormEntity(parameters));
//            HttpResponse httpResponse = httpClient.execute(httpPost);
//        }
//        catch (ClientProtocolException e)
//        {
//            e.printStackTrace();
//        }
//        catch (IOException e)
//        {
//            e.printStackTrace();
//        }

    }

    private StringBuilder inputStreamToString(InputStream is)
    {
        String line = "";
        StringBuilder total = new StringBuilder();

        // Wrap a BufferedReader around the InputStream
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));

        // Read response until the end
        try
        {
            while ((line = rd.readLine()) != null)
            {
                total.append(line);
            }
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Return full string
        return total;
    }

    protected synchronized void startPlayerHandler(String path)
    {

        try
        {
            if (mediaManager.player.isNewSession())
            {
                // Set player for recored babble
                AudioManagement.isAutoPlay = true;
                AudioManagement.isPlaying = true;
                AudioManagement.playingStatus = AudioManagement.PLAYING_AUTO;
                mediaManager.player.setOnBufferingUpdateListener(this);
                mediaManager.startPlayerHandler(path);
            }
            else
            {
                AudioManagement.isAutoPlay = false;
                AudioManagement.isPlaying = false;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            AudioManagement.isAutoPlay = false;
            AudioManagement.isPlaying = false;
        }
    }

    private final DialogInterface.OnClickListener onProcessLoadFailBabble = new DialogInterface.OnClickListener() {

        @Override
        public void onClick(DialogInterface dialog, int which)
        {
            // finish();
        }
    };

    private void getBabbleFromServer(final String physicalBabbleName, String URL)
    {

        asyncDownloadBabble = new MyAsyncBabble(context,
            new MyAsyncBabbleResponseProcess() {

                @SuppressWarnings("deprecation")
                @Override
                public void before()
                {
                    isLoading = true;
                }

                @Override
                public void after(int statusCode, String path)
                {

                    isLoading = false;
                    switch (statusCode)
                    {
                        case MyAsyncBabble.NETWORK_STATUS_OFF:
                            DialogManager.alert(context,
                                R.string.message_network_is_unavailable,
                                onProcessLoadFailBabble);
                            break;
                        case MyAsyncBabble.NETWORK_STATUS_OK:

                            // Insert babble physical file name to local
                            // database
                            try
                            {
                                if (mediaManager.isPlaying())
                                    break;
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                            }
                            startPlayerHandler(path);
                            break;
                        default:
                            DialogManager.alert(
                                context,
                                context.getString(R.string.message_please_check_sdcard),
                                onProcessLoadFailBabble);
                    }
                }
            }, physicalBabbleName,"Auto");
        asyncDownloadBabble.execute(URL);
    }

    /*
     * (non-Javadoc)
     * 
     * .
     * 
     * @see
     * android.media.MediaPlayer.OnCompletionListener#onCompletion(android.media
     * .MediaPlayer)
     */
    @Override
    public void onCompletion(MediaPlayer mp)
    {
        // Continue play babble here
//        if (GCMIntentService.notification_queue.size() > 0)
//        {
//            String _content = GCMIntentService.notification_queue.pop();
//            content = _content.split("qu;te");
//            URLPath = content[0];
//
//            // saveToHistory();
//            startPlayerHandler(URLPath);
//            if (content.length >= 2)
//            {
//                notificationID = content[1];
//                new UpdateNotification().start();
//            }
//        }
//        else
//        {
//            AudioManagement.isAutoPlay = false;
//            AudioManagement.isPlaying = false;
//            mediaManager.player.reset();
//        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * android.media.MediaPlayer.OnBufferingUpdateListener#onBufferingUpdate
     * (android.media.MediaPlayer, int)
     */
    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent)
    {
        // TODO Auto-generated method stub

    }

    class UpdateNotification extends Thread
    {
        /*
         * (non-Javadoc)
         * 
         * @see java.lang.Thread#run()
         */
        @Override
        public void run()
        {
            updateReadBabble(notificationID);
        }
    }

}
