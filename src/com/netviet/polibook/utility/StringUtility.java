/*
 * Name: $RCSfile: StringUtility.java,v $
 * Version: $Revision: 1.1 $
 * Date: $Date: Oct 31, 2011 1:54:00 PM $
 *
 * Copyright (C) 2011 COMPANY_NAME, Inc. All rights reserved.
 */

package com.netviet.polibook.utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.annotation.SuppressLint;
import android.widget.EditText;


/**
 * StringUtility class
 *
 * @author Hai Lee
 */
public final class StringUtility {
    // this format is used to set babble name
    public static final String SQL_DATE_BABBLE_NAME_FORMAT = "MMM dd, yyyy h:mm a";

    // this format is used to set date for chat with friend
    public static final String SQL_DATE_FORMAT = "MMM dd, yyy";

    // this format is used to set time for chat with friend
    public static final String SQL_TIME_FORMAT = "h:mm a";

    // this format is used to set date for public babble detail
    public static final String DATE_FORMAT = "MMMM dd, yyyy";

    // this format is used to set date for my babble, public babble, reply
    // babble
    public static final String DATE_FORMAT_PUBLIC_BABBLE = "MM/dd/yyyy";

    // this format is used to set date
    public static final String DATE_SQL_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static final String HTML_TAG_PATTERN = "<(\"[^\"]*\"|'[^']*'|[^'\">])*>";

    /**
     * Check Edit Text input string
     *
     * @param editText
     * @return
     */
    public static boolean isEmpty(EditText editText) {
        if (editText == null
                || editText.getEditableText() == null
                || editText.getEditableText().toString().trim().equalsIgnoreCase("")) {
            return true;
        }
        return false;
    }

    /**
     * Check input string
     *
     * @param editText
     * @return
     */
    public static boolean isEmpty(String editText) {
        if (editText == null || editText.trim().equalsIgnoreCase("")) {
            return true;
        }
        return false;
    }

    /**
     * Merge all elements of a string array into a string
     *
     * @param strings
     * @param separator
     * @return
     */
    public static String join(String[] strings, String separator) {
        StringBuffer sb = new StringBuffer();
        int max = strings.length;
        for (int i = 0; i < max; i++) {
            if (i != 0)
                sb.append(separator);
            sb.append(strings[i]);
        }
        return sb.toString();
    }

    /**
     * Convert current date time to string
     *
     * @return
     */
    public static String convertNowToFullDateString() {
        SimpleDateFormat dateformat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss",Locale.US);
        dateformat.setTimeZone(TimeZone.getTimeZone("GMT"));

        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        return dateformat.format(calendar.getTime());
    }

    /**
     * Convert current date time to default babble name
     *
     * @return
     */
    public static String convertNowToDefaultBabbleName() {
        SimpleDateFormat dateformat = new SimpleDateFormat(
                SQL_DATE_BABBLE_NAME_FORMAT,Locale.US);
        return dateformat.format(Calendar.getInstance().getTime());
    }

    /**
     * Convert current date time to default babble name
     *
     * @return
     */
    public static String convertNowToTimeStampString() {
        SimpleDateFormat dateformat = new SimpleDateFormat(DATE_SQL_FORMAT,Locale.US);
        return dateformat.format(Calendar.getInstance().getTime());
    }

    /**
     * Convert a time stamp string to a time stamp
     *
     * @param time
     * @return
     */
    public static Timestamp convertStringToTimeStamp(String time) {
        SimpleDateFormat dateformat = new SimpleDateFormat(DATE_SQL_FORMAT,Locale.US);
        Date date = null;
        try {
            date = dateformat.parse(time);
            return new Timestamp(date.getTime());
        } catch (ParseException e) {
            return new Timestamp(Calendar.getInstance().getTime().getTime());
        }
    }

    /**
     * Initial sync date string
     *
     * @return
     */
    public static String initDateString() {
        return "1900-01-01 09:00:00";
    }

   

    /**
     * format duration to display (mm:ss)
     *
     * @param secs
     * @return
     */
    public static String formatTimePlay(int secs) {
        int mm = secs / 60;
        int ss = secs % 60;
        Formatter fmt = new Formatter();
        fmt.format("%02d:%02d", mm, ss);
        return fmt.toString();
    }

    /**
     * Generate file name automatically based on current time stamp
     *
     * @return
     */
    public static String currentTimeString() {
        // return String.format("%d", System.currentTimeMillis());
        // vidp fix bug cannot play babble
        Calendar _calen = Calendar.getInstance();
        String _return = _calen.get(Calendar.YEAR) + "" + _calen.get(Calendar.MONTH) + ""
                + _calen.get(Calendar.DAY_OF_MONTH) + ""
                + _calen.get(Calendar.HOUR_OF_DAY) + ""
                + _calen.get(Calendar.MINUTE) + "" + _calen.get(Calendar.SECOND);
        return _return;

    }


	public static String getCurrentTime() {
        return String.format(Locale.US,"%d", System.currentTimeMillis());
    }

	public static String getDateFormatString(long timesmilis, String dateFormat) {
        SimpleDateFormat dateformat = new SimpleDateFormat(dateFormat,Locale.US);
        return dateformat.format(new Date(timesmilis));

    }

    /**
     * Convert a date string to a date
     *
     * @param time
     * @return
     */
    public static Date convertStringToDate(String time) {
        SimpleDateFormat dateformat = new SimpleDateFormat(DATE_SQL_FORMAT,Locale.US);
        Date date = null;
        try {
            date = dateformat.parse(time);
            return date;
        } catch (ParseException e) {
            return new Date(System.currentTimeMillis());
        }
    }

    public static String convertDateToString(Date date, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format,Locale.US);
        String strDate;
        try {
            strDate = dateFormat.format(date);
            return strDate;
        } catch (IllegalArgumentException e) {
            return dateFormat.format(new Date(System.currentTimeMillis()));
        }
    }

    /**
     * Validate html tag with regular expression
     *
     * @param tag html tag for validation
     * @return true valid html tag, false invalid html tag
     */
    public static boolean validateHTML(final String tag) {
        Pattern pattern = Pattern.compile(HTML_TAG_PATTERN);
        Matcher matcher = pattern.matcher(tag);
        return matcher.matches();
    }
}
