package com.netviet.polibook.utility;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.util.Log;

/**
 * @author cuongvm6037
 * 
 */
public class FileUtil {

	public static String JPEG = ".jpg";

	private File dir;

	/**
	 * Constructor
	 * 
	 * @param dir
	 *            directory where the file is stored or will be stored
	 */
	public FileUtil(File dir) {
		this.dir = dir;
	}

	/**
	 * Create new file if not exist
	 * 
	 * @param fileName
	 * @return TRUE if new file is created, FALSE if the file is already
	 *         existing
	 * @throws IOException
	 */
	public boolean createNewFile(String fileName) throws IOException {
		File f = new File(dir, fileName);
		return f.createNewFile();
	}

	/**
	 * Save image byte array into a JPEG file
	 * 
	 * @param fileName
	 * @param imageData
	 * @param quality
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public String saveImage(String fileName, byte[] imageData, int quality)
			throws FileNotFoundException, IOException {
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 5;
		Log.i("ImageSave", "File name: " + fileName);
		return saveImage(fileName, BitmapFactory.decodeByteArray(imageData, 0,
				imageData.length, options), quality);
	}

	/**
	 * Save bitmap data into a JPEG file
	 * 
	 * @param fileName
	 * @param bitmap
	 * @param quality
	 * @return absolute path to saved image
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public String saveImage(String fileName, Bitmap bitmap, int quality)
			throws FileNotFoundException, IOException {
		File f = new File(dir, fileName + JPEG);
		Log.i("ImageSave", "Save image to temporary file: "
				+ f.getAbsolutePath());
		FileOutputStream fileOutputStream = new FileOutputStream(f);

		BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);

		bitmap.compress(CompressFormat.JPEG, quality, bos);

		bos.flush();
		bos.close();
		return f.getAbsolutePath();
	}
	
	public static void renameFile(String fromFile, String toFile){
		File from      = new File(fromFile);
        File to        = new File(toFile);
        from.renameTo(to);
	}

	/**
	 * Read data from file
	 * 
	 * @param fileName
	 * @param encoding
	 * @return absolute path to saved image
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	public String readFile(String fileName, String encoding)
			throws FileNotFoundException, UnsupportedEncodingException,
			IOException {
		FileInputStream fs = new FileInputStream(new File(dir, fileName));
		BufferedReader r = new BufferedReader(new InputStreamReader(fs,
				encoding));
		char[] buf = new char[fs.available()];
		r.read(buf);
		return new String(buf);
	}

	/**
	 * Delete if file exists
	 * 
	 * @param fname
	 */
	public static void delete(String path) {
		File f = new File(path);
		if (f.exists()) {
			f.delete();
		}
	}

	/**
	 * Delete if file exists
	 * 
	 * @param f
	 */
	public static void delete(File f) {
		if (f.exists()) {
			f.delete();
		}
	}

}
