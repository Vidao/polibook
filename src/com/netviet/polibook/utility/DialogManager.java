package com.netviet.polibook.utility;

import com.netviet.polibook.R;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public final class DialogManager {
	private static AlertDialog alert = null;
	private static Dialog dialog = null;
	public static boolean sessionTimeout = false;
	private static Handler handler = new Handler();

	/**
	 * Show an alert dialog box
	 * 
	 * @param context
	 * @param message
	 */
	public static void alert(Context context, String message) {
		if (sessionTimeout)
			return;
		if (context != null) {
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
			alertDialog.setTitle(context.getString(R.string.app_name));
			alertDialog.setMessage(message);
			alertDialog.setPositiveButton(R.string.btn_ok,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface arg0, int arg1) {
						}
					});
			alertDialog.setIcon(context.getResources().getDrawable(
					R.drawable.icon));
			final AlertDialog alert = alertDialog.create();
			alert.show();
		}
	}

	/**
	 * Show an alert dialog box
	 * 
	 * @param context
	 * @param messageId
	 */
	public static void alert(Context context, int messageId) {
		if (sessionTimeout)
			return;

		if (context != null) {
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
			alertDialog.setTitle(context.getString(R.string.app_name));
			alertDialog.setMessage(context.getString(messageId));
			alertDialog.setPositiveButton(R.string.btn_ok,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface arg0, int arg1) {
						}
					});
			alertDialog.setIcon(context.getResources().getDrawable(
					R.drawable.icon));
			final AlertDialog alert = alertDialog.create();

			alert.show();

		}
	}

	/**
	 * Show an alert dialog box
	 * 
	 * @param context
	 * @param title
	 * @param message
	 */
	public static void alert(Context context, String title, String message) {
		if (sessionTimeout)
			return;

		if (context != null) {
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
			alertDialog.setTitle(title);
			alertDialog.setMessage(message);
			alertDialog.setPositiveButton(R.string.btn_ok,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface arg0, int arg1) {
						}
					});
			alertDialog.setIcon(context.getResources().getDrawable(
					R.drawable.icon));
			final AlertDialog alert = alertDialog.create();
			alert.show();
		}
	}

	/**
	 * Show an alert dialog box
	 * 
	 * @param context
	 * @param message
	 * @param positiveOnClick
	 * @param negativeOnClick
	 */
	public static void alert(Context context, String message,
			DialogInterface.OnClickListener positiveOnClick,
			DialogInterface.OnClickListener negativeOnClick) {
		if (sessionTimeout)
			return;

		if (context != null) {
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
			alertDialog.setTitle(context.getString(R.string.app_name));
			alertDialog.setMessage(message);
			alertDialog.setPositiveButton(R.string.btn_ok, positiveOnClick);
			alertDialog.setNegativeButton(R.string.slide_quit, negativeOnClick);
			alertDialog.setIcon(context.getResources().getDrawable(
					R.drawable.icon));
			final AlertDialog alert = alertDialog.create();
			alert.show();
		}
	}

	/**
	 * @param context
	 * @param message
	 * @param btnPositive
	 * @param btnNegative
	 * @param positiveOnClick
	 * @param negativeOnClick
	 */
	public static void alert(Context context, String message,
			String btnPositive, String btnNegative,
			DialogInterface.OnClickListener positiveOnClick,
			DialogInterface.OnClickListener negativeOnClick) {
		if (sessionTimeout)
			return;

		if (context != null) {
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
			alertDialog.setTitle(context.getString(R.string.app_name));
			alertDialog.setMessage(message);
			alertDialog.setPositiveButton(btnPositive, positiveOnClick);
			alertDialog.setNegativeButton(btnNegative, negativeOnClick);
			alertDialog.setIcon(context.getResources().getDrawable(
					R.drawable.icon));
			final AlertDialog alert = alertDialog.create();
			alert.show();
		}
	}

	/**
	 * Show an alert dialog box
	 * 
	 * @param context
	 * @param messageId
	 * @param positiveOnClick
	 * @param negativeOnClick
	 */
	public static void alert(Context context, int messageId,
			DialogInterface.OnClickListener positiveOnClick,
			DialogInterface.OnClickListener negativeOnClick) {
		if (sessionTimeout)
			return;

		if (context != null) {
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
			alertDialog.setTitle(context.getString(R.string.app_name));
			alertDialog.setMessage(context.getString(messageId));
			alertDialog.setPositiveButton(R.string.btn_ok, positiveOnClick);
			alertDialog.setNegativeButton(R.string.slide_quit, negativeOnClick);
			alertDialog.setIcon(context.getResources().getDrawable(
					R.drawable.icon));
			final AlertDialog alert = alertDialog.create();
			alert.show();
		}
	}

	/**
	 * Show an alert dialog box
	 * 
	 * @param context
	 * @param message
	 * @param positiveOnClick
	 */
	public static void alert(Context context, String message,
			DialogInterface.OnClickListener positiveOnClick) {

		if (sessionTimeout)
			return;

		if (context != null) {
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
			alertDialog.setTitle(context.getString(R.string.app_name));
			alertDialog.setMessage(message);
			if (positiveOnClick != null) {
				alertDialog.setPositiveButton(R.string.btn_ok, positiveOnClick);
			} else {
				alertDialog.setPositiveButton(R.string.btn_ok,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
							}
						});
			}
			alertDialog.setCancelable(false);
			alertDialog.setIcon(context.getResources().getDrawable(
					R.drawable.icon));
			final AlertDialog alert = alertDialog.create();
			alert.show();
		}
	}

	/**
	 * Show an alert dialog box
	 * 
	 * @param context
	 * @param messageId
	 * @param positiveOnClick
	 */
	public static void alert(Context context, int messageId,
			DialogInterface.OnClickListener positiveOnClick) {
		if (sessionTimeout)
			return;

		if (context != null) {
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
			alertDialog.setTitle(context.getString(R.string.app_name));
			alertDialog.setMessage(context.getString(messageId));
			alertDialog.setPositiveButton(R.string.btn_ok, positiveOnClick);
			alertDialog.setCancelable(false);
			alertDialog.setIcon(context.getResources().getDrawable(
					R.drawable.icon));
			final AlertDialog alert = alertDialog.create();
			alert.show();
		}
	}

	/**
	 * Show multiple option dialog
	 * 
	 * @param context
	 * @param messageId
	 * @param items
	 * @param selects
	 * @param positiveLabelId
	 * @param positiveOnClick
	 * @param negativeLabelId
	 * @param negativeOnClick
	 * @param itemsOnClick
	 */
	public static void showMultipleOptionDialog(Context context, int messageId,
			String[] items, boolean[] selects, int positiveLabelId,
			DialogInterface.OnClickListener positiveOnClick,
			int negativeLabelId,
			DialogInterface.OnClickListener negativeOnClick,
			DialogInterface.OnMultiChoiceClickListener itemsOnClick) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(context.getString(messageId));
		builder.setMultiChoiceItems(items, selects, itemsOnClick);
		builder.setPositiveButton(context.getString(positiveLabelId),
				positiveOnClick);
		builder.setNegativeButton(negativeLabelId, negativeOnClick);
		final AlertDialog alert = builder.create();
		handler.post(new Runnable() {

			@Override
			public void run() {
				alert.show();
			}
		});
	}

	/**
	 * Show delete babble dialog
	 * 
	 * @param positiveOnClick
	 * @param negativeOnClick
	 */
	public static void showConfirmDialog(Context context,String message,
			DialogInterface.OnClickListener positiveOnClick,
			DialogInterface.OnClickListener negativeOnClick) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(context.getString(R.string.app_name));
		builder.setMessage(message);
		builder.setPositiveButton(context.getString(R.string.btn_ok),
				positiveOnClick);
		builder.setNegativeButton(context.getString(R.string.slide_quit),
				negativeOnClick);
		final AlertDialog alert = builder.create();
		handler.post(new Runnable() {

			@Override
			public void run() {
				alert.show();
			}
		});
	}

	
	
	
	
	

	// ==================================

	/**
	 * Close popup dialog
	 */
	public static void closeDialog() {
		if (alert != null) {
			alert.dismiss();
			alert = null;
		}

		if (dialog != null) {
			dialog.dismiss();
			dialog = null;
		}
	}

	// =================================

	

	public static boolean isDismissDialog = false;

	

	
	

	

	

	public interface onSortPublicBabbleListener {
		public void sortByDate();

		public void sortByListens();

		public void sortByLikes();

		public void sortByPopularity();

	}

	public interface onDialogBabbleRowActionListener {

		/**
		 * on Synch button listener
		 * 
		 * @param babblePos
		 */
		void onSynchClick(final int babblePos);

		/**
		 * on publish button listener
		 * 
		 * @param babblePos
		 */
		void onPublishClick(final int babblePos);

		/**
		 * on replies button listener
		 * 
		 * @param babblePos
		 */
		void onRepliesClick(final int babblePos);

		/**
		 * on details button listener
		 * 
		 * @param babblePos
		 */
		void onDetailsClick(final int babblePos);

		/**
		 * on rename button listener
		 * 
		 * @param babblePos
		 */
		void onRenameClick(final int babblePos);

		/**
		 * on unpublish button listener
		 * 
		 * @param babblePos
		 */
		void onUnpublishClick(final int babblePos);

		/**
		 * on delete button listener
		 * 
		 * @param babblePos
		 */
		void onDeleteClick(final int babblePos);

		/**
		 * on delete all button listener
		 * 
		 * @param babblePos
		 */
		void onDeleteAllClick(final int babblePos);

		/**
		 * add picture to babble
		 * 
		 * @param babblePos
		 */
		void onAddPicture(final int babblePos);

		/**
		 * change picture to babble
		 * 
		 * @param babblePos
		 */
		void onChangePicture(final int babblePos);

		/**
		 * remove picture to babble
		 * 
		 * @param babblePos
		 */
		void onRemovePicture(final int babblePos);

	}
}
