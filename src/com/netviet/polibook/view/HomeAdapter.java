package com.netviet.polibook.view;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.netviet.polibook.R;
import com.netviet.polibook.model.BookModel;
import com.netviet.polibook.utility.Utility;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageSize;

public class HomeAdapter extends BaseAdapter {

	private ArrayList<BookModel> filmModelList;
	private Context context;
	DisplayImageOptions options;
	ImageSize imageSize;
	ImageLoader imageLoader = ImageLoader.getInstance();

	public HomeAdapter(Context context, ArrayList<BookModel> filmModelList) {
		this.filmModelList = filmModelList;
		this.context = context;
		Utility.setImageConfig(context, 90);
		if (imageLoader.isInited()) {
			imageLoader.destroy();
		}
		imageLoader.init(Utility.imageLoaderConfig);
		options = getImageOption();
	}

	private DisplayImageOptions getImageOption() {
		DisplayImageOptions options = new DisplayImageOptions.Builder()
				.showStubImage(R.drawable.ic_stub)
				.showImageOnFail(R.drawable.loading_false)
				.showImageForEmptyUri(R.drawable.loading_false)
				.cacheInMemory(false).cacheOnDisc(true).build();
		return options;
	}

	@Override
	public int getCount() {
		return (filmModelList == null) ? 0 : filmModelList.size();
	}

	@Override
	public Object getItem(int position) {
		return (filmModelList == null) ? null : filmModelList.get(position);
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {

		LinearLayout ll = new LinearLayout(context);
		ll.setOrientation(LinearLayout.VERTICAL);

		ImageView _bookPoster = new ImageView(context);
		BookModel _model = filmModelList.get(position);
		String _filmThumbnail = "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSBYVn15rWW5A8ydapbaMU-5YgT868sRBo0fhmVqJIAJu90nKgi";
		_bookPoster.setScaleType(ImageView.ScaleType.FIT_XY);
		loadingImage(imageSize, _filmThumbnail, _bookPoster);

		// _filmPoster.setLayoutParams(new Gallery.LayoutParams(
		// LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		ll.addView(_bookPoster);

		String _filmTitle = _model.getName();
		TextView tv = new TextView(ll.getContext());

		tv.setTag(_filmTitle);
		tv.setText(_filmTitle);
		tv.setTextColor(Color.WHITE);
		tv.setLayoutParams(new Gallery.LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT));
		ll.addView(tv);
		// The preferred Gallery item background
		// i.setBackgroundResource(mGalleryItemBackground);
		return ll;

	}

	private void loadingImage(ImageSize targetSize, String pictureHref,
			final ImageView imageView) {
		imageLoader.displayImage(pictureHref, imageView);
		// imageLoader.loadImage(pictureHref, targetSize, options,
		// new SimpleImageLoadingListener() {
		// @Override
		// public void onLoadingStarted(String imageUri, View view) {
		// imageView.setImageResource(R.drawable.ic_stub);
		// super.onLoadingStarted(imageUri, view);
		// }
		//
		// @Override
		// public void onLoadingComplete(String imageUri, View view,
		// Bitmap loadedImage) {
		// // Do whatever you want with Bitmap
		// imageView.setImageBitmap(loadedImage);
		// imageView.invalidate();
		// }
		// });
	}

}
