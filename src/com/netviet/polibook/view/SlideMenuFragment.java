package com.netviet.polibook.view;

import com.netviet.polibook.ApplicationClass;
import com.netviet.polibook.R;
import com.netviet.polibook.func.BaseSlideActivity;
import com.netviet.polibook.iview.ISlideLeftMenuListenner;
import com.netviet.polibook.ui.RoundedImageView;
import com.netviet.polibook.utility.NetworkUtility;
import com.netviet.polibook.utility.PreferencesManager;
import com.netviet.polibook.utility.Utility;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageSize;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public class SlideMenuFragment extends Fragment implements OnClickListener {
	ISlideLeftMenuListenner callback;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	private LinearLayout.LayoutParams params;

	private DisplayImageOptions options;
	private ImageSize imageSize;
	private ImageLoader imageLoader = ImageLoader.getInstance();

	public void setContext(BaseSlideActivity self) {
		if (NetworkUtility.getInstance(self).isNetworkAvailable()) {
			// load image
			Utility.setImageConfig(self, 25);
			if (imageLoader.isInited()) {
				imageLoader.destroy();
			}
			imageLoader.init(Utility.imageLoaderConfig);
			options = getImageOption();
			params = new LinearLayout.LayoutParams(25, 25);
			imageSize = new ImageSize(25, 25);
			PreferencesManager _pref = PreferencesManager.getInstance(self);
			String avatarHref = _pref
					.getStringValue(PreferencesManager.PREF_USER_AVARTAR_HREF);
			RoundedImageView imgAvatar = (RoundedImageView) mainLayout
					.findViewById(R.id.iv_avatar);

			if (avatarHref != null && avatarHref.length() > 5) {
				avatarHref = ApplicationClass.ROOT_DATA_URL + avatarHref;
				imageLoader.displayImage(avatarHref, imgAvatar, options);
			}
		}

	}

	private DisplayImageOptions getImageOption() {
		DisplayImageOptions options = new DisplayImageOptions.Builder()
				.showStubImage(R.drawable.ic_stub)
				.showImageOnFail(R.drawable.loading_false)
				.showImageForEmptyUri(R.drawable.loading_false)
				.cacheInMemory(true).cacheOnDisc(true).build();
		return options;
	}

	private LinearLayout mainLayout;
	private LinearLayout homeLayout;
	private LinearLayout profileLayout;
	private LinearLayout preferenceLayout;
	private LinearLayout listCustomerLayout;
	private LinearLayout quitLayout;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mainLayout = (LinearLayout) inflater.inflate(R.layout.slide_menu,
				container, false);
		homeLayout = (LinearLayout) mainLayout.findViewById(R.id.layout_home);
		profileLayout = (LinearLayout) mainLayout
				.findViewById(R.id.layout_profile);

		preferenceLayout = (LinearLayout) mainLayout
				.findViewById(R.id.layout_preference);

		listCustomerLayout = (LinearLayout) mainLayout
				.findViewById(R.id.layout_listcustomer);

		quitLayout = (LinearLayout) mainLayout.findViewById(R.id.layout_quit);

		homeLayout.setOnClickListener(this);
		profileLayout.setOnClickListener(this);
		preferenceLayout.setOnClickListener(this);
		listCustomerLayout.setOnClickListener(this);
		quitLayout.setOnClickListener(this);

		return mainLayout;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int _viewID = v.getId();
		switch (_viewID) {
		case R.id.layout_home:
			callback.homeSelectedListener();
			break;
		case R.id.layout_profile:
			callback.profileSelectedListener();
			break;
		case R.id.layout_preference:
			callback.preferenceSelectedListener();
			break;
		case R.id.layout_listcustomer:
			callback.listCustomerSelectedListener();
			break;
		case R.id.layout_quit:
			callback.quitSelectedListener();
			break;
		default:
			break;
		}

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			callback = (ISlideLeftMenuListenner) activity;
		} catch (Exception e) {
		}
	}

}
