package com.netviet.polibook.view;

import java.util.Random;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * A view that displays audio data on the screen as a waveform.
 */
public class WaveformView extends SurfaceView {

	private final static int DISTANCE_AMPLITUDE = 20;
	private static int NUMBER_WAVE = 1;
	private final static float TWO_PI = (float) (2 * Math.PI);

	private Paint mPaint;
	private Random mRandom;

	private int mAudioVolumeLevel;
	private float mWLength;
	private int mNumOfMainWaveLoops;
	private int mRandWaveLoops;

	private float mMeasuredWidth;
	private float mMeasuredHeight;
	private float mHalfHeight;

	private float mWRadUnit;
	private float mWRads;

	private float mCoordinatesX;
	private float mCoordinatesY;
	private float mOldCoordinatesX;
	private float mOldCoordinatesY;

	private float mTempAmp;
	private float mAmplitude;
	private int mWSpace;
	private boolean mIsSurfaceCreated = false;
	private static int mWCount;
	private int waveType = 0;

	public int getWaveType() {
		return waveType;
	}

	public void setWaveType(int waveType) {
		this.waveType = waveType;
	}

	public WaveformView(Context context) {
		this(context, null, 0);
		configWaveform();
	}

	public WaveformView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
		configWaveform();
	}

	public WaveformView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		configWaveform();
	}

	private boolean init = true;

	private void configWaveform() {
		mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		mRandom = new Random();
		mAudioVolumeLevel = 0;
		mNumOfMainWaveLoops = 3;

		mWSpace = 2;
		mWCount = 0;

		mOldCoordinatesX = 0;
		mOldCoordinatesY = 0;

		getHolder().addCallback(new SurfaceHolder.Callback() {
			@Override
			public void surfaceCreated(SurfaceHolder holder) {
				mIsSurfaceCreated = true;
				updateAudioVolumeLevel(0);
			}

			@Override
			public void surfaceChanged(SurfaceHolder holder, int format,
					int width, int height) {

			}

			@Override
			public void surfaceDestroyed(SurfaceHolder holder) {

			}
		});

	}

	private boolean isMaxVolumn = false;

	public synchronized void updateAudioVolumeLevel(int volume) {
		// Update the display.
		if (!mIsSurfaceCreated) {
			return;
		}
		if (init) {

			Canvas canvas = getHolder().lockCanvas();
			Paint test = new Paint();
			test.setStrokeWidth(5);
			test.setColor(Color.WHITE);
			canvas.drawLine(0, getHeight() / 2, getWidth(), getHeight() / 2,
					test);
			getHolder().unlockCanvasAndPost(canvas);
			init = false;
			return;
		}
		if (volume < 300) {
			isMaxVolumn = false;
		} else {
			isMaxVolumn = true;
		}
		Canvas canvas = getHolder().lockCanvas();
		if (canvas != null) {

			if (volume < mAudioVolumeLevel) {
				// if volume is decreased, we need reduce amplitude step by step
				this.mAudioVolumeLevel -= DISTANCE_AMPLITUDE;

				if (mAudioVolumeLevel < DISTANCE_AMPLITUDE) {
					this.mAudioVolumeLevel = 0;
				}
			} else {
				this.mAudioVolumeLevel = volume;
			}

			if (mWCount < getWidth()) {
				mWCount += mWSpace + 10;
			} else {
				mWCount = 0;
			}
			// NUMBER_WAVE = 3;
			drawWave(canvas);
			getHolder().unlockCanvasAndPost(canvas);
		}
	}

	public void drawWave(Canvas canvas) {

		canvas.drawColor(Color.BLACK);

		mMeasuredWidth = getMeasuredWidth();
		mMeasuredHeight = getMeasuredHeight();
		mHalfHeight = mMeasuredHeight / 2;
		mWLength = (float) (2 * mMeasuredWidth / mNumOfMainWaveLoops);

		for (int i = 0; i < NUMBER_WAVE; i++) {

			mAmplitude = (float) (mAudioVolumeLevel * (10 - 1.5 * i) / 10);
			// if (i == 0 || true) {
			// Main wave form
			mRandWaveLoops = mNumOfMainWaveLoops;
			mWRadUnit = TWO_PI * 2 / mWLength;
			// }
			// else {
			// // Secondary waves form
			// mRandWaveLoops = mRandom.nextInt(5);
			// mWRadUnit = (float) (Math.pow(-1, mRandom.nextInt(2)) * TWO_PI
			// * mWSpace / mWLength);
			// }

			// mTempAmp = 150;
			// mTempAmp = mAmplitude;
			for (float j = 0; j < (mMeasuredWidth); j += mWSpace) {
				mCoordinatesX = j;
				// if(mCoordinatesX>mMeasuredWidth){
				// break;
				// }

				mWRads = (mWCount - j) * mWRadUnit;

				// float kk = Math.sin(mWRads)
				mTempAmp = (float) (mAmplitude * (1 - Math
						.abs((mCoordinatesX / mMeasuredWidth) - .5) * 2));
				mCoordinatesY = (float) (Math.sin(mWRads) * mTempAmp);
				if (mCoordinatesX == mMeasuredWidth / 2) {

					float _mWRads1 = (mWCount - (j - mWSpace)) * mWRadUnit;
					float _mWRads2 = (mWCount - (j + mWSpace)) * mWRadUnit;
					float y1 = (float) (Math.sin(_mWRads1) * mTempAmp);
					float y2 = (float) (Math.sin(_mWRads2) * mTempAmp);
					mCoordinatesY = (y1 + y2) / 2;

				}

				// mCoordinatesY = (float) (Math.sin(mCoordinatesX + .4f) *
				// mTempAmp);
				if (mCoordinatesX != mMeasuredWidth / 2
						|| mCoordinatesY != mAmplitude) {
					if (mCoordinatesX != 0) {

						if (i != 0) {
							// canvas.drawLine(mOldCoordinatesX,
							// mOldCoordinatesY,
							// mCoordinatesX + .4f, mHalfHeight
							// + mCoordinatesY, mPaint);
						} else {
							if (!isMaxVolumn) {
								mPaint.setColor(Color.WHITE);
							} else {
								mPaint.setColor(Color.GREEN);
							}
							mPaint.setStrokeWidth(10);
							mPaint.setAlpha(255);
							if (waveType == 1) {// eye wave
								mCoordinatesY = (float) (Math
										.sin(mCoordinatesX + .4f) * mTempAmp);
							}
							canvas.drawLine(mOldCoordinatesX, mOldCoordinatesY,
									mCoordinatesX + .4f, mHalfHeight
											+ mCoordinatesY, mPaint);
							mPaint.setColor(Color.WHITE);
							mPaint.setStrokeWidth(3);
							mPaint.setAlpha(90);
							if (waveType == 2) {
								canvas.drawLine(mOldCoordinatesX,
										mOldCoordinatesY, mCoordinatesX, mHalfHeight, mPaint);
							}
						}

					}
					mOldCoordinatesX = mCoordinatesX;
					mOldCoordinatesY = mHalfHeight + mCoordinatesY;
				}
			}
		}

	}
}
