package com.netviet.polibook.view;

import java.util.ArrayList;

import com.netviet.polibook.R;
import com.netviet.polibook.model.BookModel;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

public class HomeFragments extends Fragment {

	private static final String KEY_CONTENT = "TestFragment:Content";

	public static HomeFragments newInstance(String content) {
		HomeFragments fragment = new HomeFragments();
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < 20; i++) {
			builder.append(content).append(" ");
		}
		builder.deleteCharAt(builder.length() - 1);
		fragment.mContent = builder.toString();
		return fragment;
	}

	private String mContent = "???";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if ((savedInstanceState != null)
				&& savedInstanceState.containsKey(KEY_CONTENT)) {
			mContent = savedInstanceState.getString(KEY_CONTENT);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		LinearLayout _layout = (LinearLayout) inflater.inflate(
				R.layout.book_normal_fragments, container, false);
		GridView _gallery = (GridView) _layout.findViewById(R.id.gv_related);
		initData();
		HomeAdapter _adapter = new HomeAdapter(getActivity(), listFilmData);
		_gallery.setAdapter(_adapter);
		return _layout;
	}

	private ArrayList<BookModel> listFilmData = new ArrayList<BookModel>();

	private void initData() {
		BookModel _model = new BookModel();
		_model.setAuthor("Linh Thú");
		_model.setName("Sách nhạy cảm");
		_model.setThumbnail("https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSBYVn15rWW5A8ydapbaMU-5YgT868sRBo0fhmVqJIAJu90nKgi");
		listFilmData.clear();
		for (int i = 0; i < 10; i++) {
			listFilmData.add(_model);
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString(KEY_CONTENT, mContent);
	}

}
