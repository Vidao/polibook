/*
 * Name: $RCSfile: ChipsMultiAutoCompleteTextview.java,v $
 * Version: $Revision: 1.1 $
 * Date: $Date: Apr 4, 2013 3:01:57 PM $
 *
 * Copyright (C) 2012 COMPANY NAME, Inc. All rights reserved.
 */
package com.netviet.polibook.ui;

/**
 * @author vidp requiment: android 2.3.3 or higher
 *
 */

import java.util.ArrayList;

import com.netviet.polibook.ApplicationClass;
import com.netviet.polibook.R;
import com.netviet.polibook.utility.Utility;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.method.KeyListener;
import android.text.style.ClickableSpan;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;
import android.view.View.OnKeyListener;

public class ChipsMultiAutoCompleteTextview extends MultiAutoCompleteTextView
        implements OnItemClickListener
{

    private final String TAG = "ChipsMultiAutoCompleteTextview";
    private String itemContent;
    private String itemDeleteClickContent;
    private String itemAddContent;
    private Handler parentHanlder;
    private int iconSize;
    private Context context;
    private boolean suggest_flag = true;

    /**
     * @return the suggest_flag
     */
    public boolean isSuggest_flag()
    {
        return suggest_flag;
    }

    /**
     * @param suggest_flag the suggest_flag to set
     */
    public void setSuggest_flag(boolean suggest_flag)
    {
        this.suggest_flag = suggest_flag;
    }


    /**
     * @return the parentHanlder
     */
    public Handler getParentHanlder()
    {
        return parentHanlder;
    }

    /**
     * @param parentHanlder the parentHanlder to set
     */
    public void setParentHanlder(Handler parentHanlder)
    {
        this.parentHanlder = parentHanlder;
    }

    /**
     * @return the itemDeleteClickContent
     */
    public String getItemDeleteClickContent()
    {
        return itemDeleteClickContent;
    }

    /**
     * @param itemDeleteClickContent the itemDeleteClickContent to set
     */
    public void setItemDeleteClickContent(String itemDeleteClickContent)
    {
        this.itemDeleteClickContent = itemDeleteClickContent;
    }

    /**
     * @return the itemAddContent
     */
    public String getItemAddContent()
    {
        return itemAddContent;
    }

    /**
     * @param itemAddContent the itemAddContent to set
     */
    public void setItemAddContent(String itemAddContent)
    {
        this.itemAddContent = itemAddContent;
    }

    /**
     * @return the itemContent
     */
    public String getItemContent()
    {
        return itemContent;
    }

    /**
     * @param itemContent the itemContent to set
     */
    public void setItemContent(String itemContent)
    {
        this.itemContent = itemContent;
    }

    /* Constructor */
    public ChipsMultiAutoCompleteTextview(Context context)
    {
        super(context);
        init(context);

    }

    /* Constructor */
    public ChipsMultiAutoCompleteTextview(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init(context);
    }

    /* Constructor */
    public ChipsMultiAutoCompleteTextview(Context context, AttributeSet attrs,
        int defStyle)
    {
        super(context, attrs, defStyle);
        init(context);
    }

    /* set listeners for item click and text change */
    public void init(Context context)
    {
        setOnItemClickListener(this);
        addTextChangedListener(textWather);
        iconSize = (int) Utility.convertDpToPixel(20, context);
        this.context = context;
    }

    /*
     * TextWatcher, If user type any friends name and press comma then following
     * code will regenerate chips
     */
    private TextWatcher textWather = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before,
            int count)
        {

            if (count >= 1)
            {
                if (s.charAt(start) == ',')
                {
                    String _currentText = getText().toString();
                    if (_currentText.indexOf(",") == 0)
                    {
                        setText(_currentText.substring(1, _currentText.length()));
                        return;
                    }
                    String _addedText = (getText() + "").replaceAll(
                        lastestString, "");
                    if (itemContent == null && suggest_flag)
                    {
                        return;
                    }
                    // problem because of lastTest string;

                    if (((itemContent + ",").indexOf(_addedText) != -1 || !suggest_flag)
                        && lastestString.indexOf(_addedText) == -1)
                    {
                        if (suggest_flag
                            || (!suggest_flag && ApplicationClass.isValidEmail(_addedText.substring(
                                0, _addedText.length() - 1))))
                        {

                            setItemAddContent(_addedText.substring(0,
                                _addedText.length() - 1));
//                            parentHanlder.sendEmptyMessage(0);
                            setChips();
                        }
                        
                    }
                    else
                    {
                        setText(lastestString);
                        setChips();
                    }
                }
                else
                {
                }
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
            int after)
        {}

        @Override
        public void afterTextChanged(Editable s)
        {}
    };

    /* This function has whole logic for chips generate */
    public void setChips()
    {
        if (getText().toString().contains(","))
        {
            SpannableStringBuilder ssb = new SpannableStringBuilder(getText());
            String chips[] = getText().toString().trim().split(",");

            String lastChip = chips[chips.length - 1];
            int x = 0;
            for (final String c : chips)
            {
                
                // inflate chips_edittext layout
                LayoutInflater lf = (LayoutInflater) getContext().getSystemService(
                    Activity.LAYOUT_INFLATER_SERVICE);
                TextView textView = (TextView) lf.inflate(
                    R.layout.chips_edittext, null);
                textView.setText(c);
                // if (app.deviceInfo.getWidth() >= 400)
                // {
                textView.setTextSize(20);
                iconSize = (int) Utility.convertDpToPixel(30, context);
                // }
                setFlags(textView, c);
                int spec = MeasureSpec.makeMeasureSpec(0,
                    MeasureSpec.UNSPECIFIED);
                textView.measure(spec, spec);
                textView.layout(0, 0, textView.getMeasuredWidth(),
                    textView.getMeasuredHeight());
                Bitmap b = Bitmap.createBitmap(textView.getWidth(),
                    textView.getHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(b);
                canvas.translate(-textView.getScrollX(), -textView.getScrollY());
                textView.draw(canvas);
                textView.setDrawingCacheEnabled(true);
                Bitmap cacheBmp = textView.getDrawingCache();
                Bitmap viewBmp = cacheBmp.copy(Bitmap.Config.ARGB_8888, false);
                cacheBmp = null;
                textView.setDrawingCacheEnabled(false);
                textView.destroyDrawingCache();
                @SuppressWarnings("deprecation")
                BitmapDrawable bmpDrawable = new BitmapDrawable(viewBmp);
                // recount heigh for small screen;
                int _heighDIP = Utility.convertDips2Pixels(
                    bmpDrawable.getIntrinsicHeight(), context);
                _heighDIP = _heighDIP < bmpDrawable.getIntrinsicHeight() ? _heighDIP
                    : bmpDrawable.getIntrinsicHeight();
                bmpDrawable.setBounds(0, 0, bmpDrawable.getIntrinsicWidth(),
                    _heighDIP);
                ImageSpan _imageSpan = new ImageSpan(bmpDrawable);
                ssb.setSpan(_imageSpan, x, x + c.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                final int _startPoint = x >= 0 ? x : 0;
                ClickableSpan _clickSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View widget)
                    {
                        // TODO Auto-generated method stub
                        removeChip(_startPoint, c.length());
                    }
                };
                // ssb.setSpan(_clickSpan, _startPoint, _startPoint + 1,
                // Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                ssb.setSpan(_clickSpan, _startPoint, _startPoint,
                    Spannable.SPAN_MARK_MARK);
                x = x + c.length() + 1;
            }
            // set chips span
            setText(ssb);
            // move cursor to last;
            setSelection(getText().length());
            // buffer lastestString;
            lastestString = getText() + "";
        }

    }

    private void removeChip(int startPoint, int length)
    {

        String _textContent = getText() + "";
        int endpoint = (startPoint + length + 1) < _textContent.length() ? (startPoint
            + length + 1)
            : _textContent.length();
        String _textRemove = _textContent.substring(startPoint, endpoint - 1).trim();
        setItemDeleteClickContent(_textRemove);
//        parentHanlder.sendEmptyMessage(GroupMakerActivity.HANDLER_REMOVE_FRIEND);
        _textContent = _textContent.substring(0, startPoint)
            + _textContent.substring(endpoint);
        _textContent = _textContent.trim();
        if (_textContent.length() <= 2)
        {
            _textContent = "";
        }
        lastestString = _textContent;
        setText(_textContent);
        setChips();
    }

    public String lastestString = "";

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
        long id)
    {
        String _newText = getText() + "";

        String _addedText = _newText.replaceAll(lastestString, "").trim();

        if (lastestString.indexOf(_addedText) != -1)
        {
            setText(lastestString);
        }
        else
        {
            setItemAddContent(_addedText.substring(0, _addedText.length() - 1));
//            parentHanlder.sendEmptyMessage(GroupMakerActivity.HANDLER_ADD_FRIEND);
        }
        setChips();
    }

    @Override
    public void onSelectionChanged(int start, int end)
    {

        CharSequence text = getText();
        if (text != null)
        {
            if (start != text.length() || end != text.length())
            {
                setSelection(text.length(), text.length());
                return;
            }
        }

        super.onSelectionChanged(start, end);
    }

    /*
     * this method set country flag image in textview's drawable component, this
     * logic is not optimize, you need to change as per your requirement
     */
    public void setFlags(TextView textView, String country)
    {
        Drawable img;
        Resources res = getResources();
        img = res.getDrawable(R.drawable.delete_user);
        img.setBounds(0, 0, iconSize, iconSize);

        textView.setCompoundDrawables(img, null, null, null);
    }

}
