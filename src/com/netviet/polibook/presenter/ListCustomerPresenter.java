package com.netviet.polibook.presenter;

import com.netviet.polibook.iview.IListCustomer;
import com.netviet.polibook.model.CustomerModel;


public class ListCustomerPresenter {
	
	IListCustomer iListView;
	public ListCustomerPresenter(IListCustomer ilistCustomer){
		iListView = ilistCustomer;
	}
	
	public void add(CustomerModel model){
		iListView.addCustomer(model);
	}
	
	public void remove(int index){
		iListView.removeCustomer(index);
	}
	
	public void removeAll(){
		iListView.removeAll();
	}

}
