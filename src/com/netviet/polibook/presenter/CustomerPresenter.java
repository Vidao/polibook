package com.netviet.polibook.presenter;

import com.netviet.polibook.imodel.ICustomerModel;
import com.netviet.polibook.iview.ICustomerView;
import com.netviet.polibook.model.CustomerModel;



public class CustomerPresenter {
	private ICustomerView view;

	public CustomerPresenter( ICustomerView view) {
		this.view = view;
	}

	public void setView(ICustomerModel model) {
		if (view != null) {
			view.setAge(model.getCustomerAge());
			view.setID(model.getCustomerID());
			view.setName(model.getCustomerName());
			
		}
	}
	
	public void saveModel(ICustomerModel _refModel){
		int _oldAge = _refModel.getCustomerAge(); 
		_refModel.setCustomerAge(view.getAge());
		_oldAge =  view.getAge() -_oldAge;
		int _birth = _refModel.getCustomerBirth() + _oldAge;
		_refModel.setCustomerBirth(_birth);
		_refModel.setCustomerID(view.getID());
		_refModel.setCustomerName(view.getName());
	}
	
}
