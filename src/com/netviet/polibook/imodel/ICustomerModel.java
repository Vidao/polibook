package com.netviet.polibook.imodel;

public interface ICustomerModel {

	public void setCustomerBirth(int bith);

	public void setCustomerName(String name);

	public void setCustomerID(String ID);

	public void setCustomerAge(int age);

	public void setData(String customerName, String customerID, int customerAge);

	public int getCustomerAge();

	public String getCustomerID();

	public String getCustomerName();
	
	public int getCustomerBirth();

}
