package com.netviet.polibook.model;

import java.util.Observable;

import com.netviet.polibook.imodel.ICustomerModel;


public class CustomerModel extends Observable implements ICustomerModel{
	private String customerID;
	private int customerAge;
	private String customerName;
	private int customerBirth = 0;

	/**
	 * @return the customerBirth
	 */
	@Override
	public int getCustomerBirth() {
		return customerBirth;
	}

	/**
	 * @param customerBirth the customerBirth to set
	 */
	public void setCustomerBirth(int customerBirth) {
		this.customerBirth = customerBirth;
	}

	/**
	 * @return the customerID
	 */
	public String getCustomerID() {
		return customerID;
	}

	/**
	 * @param customerID
	 *            the customerID to set
	 */
	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	/**
	 * @return the customerAge
	 */
	public int getCustomerAge() {
		return customerAge;
	}

	/**
	 * @param customerAge
	 *            the customerAge to set
	 */
	@Override
	public void setCustomerAge(int customerAge) {
		this.customerAge = customerAge;
	}

	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName
	 *            the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	public void setData(String customerName, String customerID, int customerAge){
		this.customerAge = customerAge;
		this.customerID = customerID;
		this.customerName = customerName;
		triggerObservers();
	}

	public void triggerObservers() {
		setChanged();
		notifyObservers();
	}

	

}
