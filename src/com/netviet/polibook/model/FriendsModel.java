package com.netviet.polibook.model;

public class FriendsModel {

	public FriendsModel() {

	}

	private String friendName;
	private int friendID;
	private String friendAvataHref;
	private int numberOfUnreadMessage;
	private int ownerID = 0;
	private boolean isMarker = false;
	

	public boolean isMarker() {
		return isMarker;
	}

	public void setMarker(boolean isMarker) {
		this.isMarker = isMarker;
	}

	public int getOwnerID() {
		return ownerID;
	}

	public void setOwnerID(int ownerID) {
		this.ownerID = ownerID;
	}


	public int getNumberOfUnreadMessage() {
		return numberOfUnreadMessage;
	}

	public void setNumberOfUnreadMessage(int numberOfUnreadMessage) {
		this.numberOfUnreadMessage = numberOfUnreadMessage;
	}

	public String getFriendName() {
		return friendName;
	}

	public void setFriendName(String friendName) {
		this.friendName = friendName;
	}

	public int getFriendID() {
		return friendID;
	}

	public void setFriendID(int friendID) {
		this.friendID = friendID;
	}

	public String getFriendAvataHref() {
		return friendAvataHref;
	}

	public void setFriendAvataHref(String friendAvataHref) {
		this.friendAvataHref = friendAvataHref;
	}

}
