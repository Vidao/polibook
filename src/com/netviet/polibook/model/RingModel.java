package com.netviet.polibook.model;

public class RingModel {
	
	private int ringID;
	private String ringName;
	private String ownerID;
	private int groupID;
	private int duration;
	private String URLHref;
	private String imageHref;
	private boolean isOwner;
	private int numberOfListen = 0;
	private int numberOfLike = 0;
	private int numberOfReply = 0;
	private String ownerName = "";
	private String createdDate = "";
	private String localFilePath = "";
	
	
	


	public String getLocalFilePath() {
		return localFilePath;
	}


	public void setLocalFilePath(String localFilePath) {
		this.localFilePath = localFilePath;
	}


	public String getCreatedDate() {
		return createdDate;
	}


	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}


	public String getOwnerName() {
		return ownerName;
	}


	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}


	public int getNumberOfReply() {
		return numberOfReply;
	}


	public void setNumberOfReply(int numberOfReply) {
		this.numberOfReply = numberOfReply;
	}


	public int getNumberOfListen() {
		return numberOfListen;
	}


	public void setNumberOfListen(int numberOfListen) {
		this.numberOfListen = numberOfListen;
	}


	public int getNumberOfLike() {
		return numberOfLike;
	}


	public void setNumberOfLike(int numberOfLike) {
		this.numberOfLike = numberOfLike;
	}


	public boolean isOwner() {
		return isOwner;
	}


	public void setOwner(boolean isOwner) {
		this.isOwner = isOwner;
	}


	public int getRingID() {
		return ringID;
	}


	public void setRingID(int ringID) {
		this.ringID = ringID;
	}


	public String getRingName() {
		return ringName;
	}


	public void setRingName(String ringName) {
		this.ringName = ringName;
	}


	public String getOwnerID() {
		return ownerID;
	}


	public void setOwnerID(String ownerID) {
		this.ownerID = ownerID;
	}


	public int getGroupID() {
		return groupID;
	}


	public void setGroupID(int groupID) {
		this.groupID = groupID;
	}


	public int getDuration() {
		return duration;
	}


	public void setDuration(int duration) {
		this.duration = duration;
	}


	public String getURLHref() {
		return URLHref;
	}


	public void setURLHref(String uRLHref) {
		URLHref = uRLHref;
	}


	public String getImageHref() {
		return imageHref;
	}


	public void setImageHref(String imageHref) {
		this.imageHref = imageHref;
	}


	public RingModel(){
		
	}

}
