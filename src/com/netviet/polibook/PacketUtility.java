/*
 * Name: $RCSfile: PacketUtility.java,v $
 * Version: $Revision: 1.1 $
 * Date: $Date: Nov 15, 2011 2:05:59 PM $
 *
 * Copyright (C) 2011 COMPANY_NAME, Inc. All rights reserved.
 */

package com.netviet.polibook;

/**
 * PacketUtility
 *
 * @author Hai Le
 * @note by nhanhk: should be placed here
 */
public class PacketUtility {
    private static PacketUtility instance = null;

    /**
     * Constructor
     *
     * @param context
     */
    public PacketUtility() {
    }

    /**
     * Get class instance
     *
     * @return
     */
    public static PacketUtility getInstance() {
        if (instance == null) {
            instance = new PacketUtility();
        }
        return instance;
    }

    /**
     * Get package name
     *
     * @return
     */
    public String getPacketName() {
        return this.getClass().getPackage().getName();
    }

    /**
     * Get package name following user id
     *
     * @param userId
     * @return
     */
    public String getPacketName(String userId) {
        return this.getClass().getPackage().getName() + "/" + userId;
    }
}
