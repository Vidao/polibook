package com.netviet.polibook;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Environment;

public class ApplicationClass extends Application {

	/**
	 * Constructor
	 */
	public ApplicationClass() {
		super();
	}

	@Override
	public void onCreate() {
//		appContex = getApplicationContext();
	}

//	private Context appContex;
	public static  int SCREEN_WIDTH = 0;
	public static  int SCREEN_HEIGH = 0;
	public static final String FILE_RAW_EXTENSION = ".raw";
	public static final String FILE_WAV_EXTENSION = ".wav";
	public static final String FILE_MP3_EXTENSION = ".mp3";
	public static final String FILE_JPEG_EXTENSION = ".jpeg";
	public static final String TEMP_FILE_RAW = "temp" + FILE_RAW_EXTENSION;
	public static final int STATUS_READY_TO_PLAY = 0;
	public static final int STATUS_RECORDING = 1;
	public static final int STATUS_PLAYING = 2;
	public static final int STATUS_STARTING = 3;
	public static final int STATUS_LOADING = 4;
	public static final int VOICE_NEW = 0;
	public static final int VOICE_SAVED = 1;
	public static final int VOICE_CANCELED = 2;
	public static final int MAX_RECORD_TIME = 60;
	public static final String PROJECT_NUMBER = "200306111040";
	public static final String PROJECT_ID = "coral-circlet-448";
	public static final String DISPLAY_MESSAGE_ACTION = "com.dao.lovelyring.DISPLAY_MESSAGE";
	public static final String ROOT_MEMORY_PATH = Environment.getExternalStorageDirectory()
			.getAbsolutePath()
			+ "/lovelyrings/";
	public static final String NOTIFICATION_DISPLAY_MESSAGE_FILTER = "COM.DAO.LOVELYRING_NOTIFY_MESSAGE";
//	public static final String FACEBOOK_APP_ID = "cHd0KXWEkXYS5LjVXr+aJS1FEWQ=";
	public int meterLevel = 0;
	public boolean busy = false;
	public int size = 0;
	public boolean updated = false;
	public String currentFilePath;
	public int currentFileStatus = VOICE_NEW;
	public int status = STATUS_READY_TO_PLAY;
	public static final String URL_API = "http://192.168.1.12:8080/LovelyRing/";
	public static final String ROOT_DATA_URL = "http://192.168.1.12:8080/";
//	public static final String URL_API = "http://192.168.1.104:8080/LovelyRing/";
//	public static final String ROOT_DATA_URL = "http://192.168.1.104:8080/";
	public static final String APP_ICON_URL = URL_API+"icon.png";
	/*
	 * Http params
	 */

	// PARAM METHOD
	public static final String PARAM_METHOD_FRIEND_LIST = "ListFriends";
	public static final String PARAM_METHOD_FRIENDS_OF_FRIEND = "FsOfFriend";
	public static final String PARAM_METHOD_PARTICIPANT_LIST = "ListParticipant";
	public static final String PARAM_METHOD_GROUP_LIST = "ListGroup";
	public static final String PARAM_METHOD_SIGNUP = "SignUp";
	public static final String PARAM_METHOD_LOGIN = "Login";
	public static final String PARAM_METHOD_UPDATE_DEVICE_TOKEN = "setDeviceToken";
	public static final String PARAM_METHOD_FRIENDS_REQUEST = "ListRequest";
	public static final String PARAM_METHOD_SONG_REQUEST = "ListSong";
	
	public static final String PARAM_METHOD_FRIEND_REQUEST = "MakeFriend";
	public static final String PARAM_METHOD_FRIEND_ACCEPTED = "AcceptFriend";
	public static final String PARAM_METHOD_FRIEND_REJECTED = "RejectFriend";
	public static final String PARAM_METHOD_DELETE_PARTICIPANT = "DeleteParticipant";
	public static final String PARAM_METHOD_ADD_PARTICIPANTS = "AddParticipants";
	public static final String PARAM_METHOD_UPLOAD_FILE = "UploadFile";
	public static final String PARAM_METHOD_CREATE_GROUP = "CreateGroup";
	public static final String PARAM_METHOD_UPLOAD_RING = "UploadRing";
	public static final String PARAM_METHOD_LIST_RING = "ListRing";
	public static final String PARAM_METHOD_LIST_GLOBALRING = "ListGlobalRing";
	public static final String PARAM_METHOD_LIKE = "Like";
	public static final String PARAM_METHOD_LISTEN = "Listen";
	public static final String PARAM_METHOD_RENAME_GROUP = "RenameGroup";
	public static final String PARAM_METHOD_DELETE_GROUP = "DeleteGroup";
	public static final String PARAM_METHOD_LEAVE_GROUP = "LeaveGroup";
	// PARAM INPUT
	public static final String PARAM_INPUT_USER_TOKEN = "tk";
	
	public static final String PARAM_INPUT_DURATION = "dr";
	public static final String PARAM_INPUT_GROUP_ID = "gid";
	public static final String PARAM_INPUT_GROUP_NAME = "gname";
	public static final String PARAM_INPUT_USER_MEMBER_ID = "umid";
	public static final String PARAM_INPUT_USER_MEMBER_NAME = "umname";
	
	public static final String PARAM_INPUT_RING_NAME = "rin";
	public static final String PARAM_INPUT_RING_ID = "riid";
	public static final String PARAM_INPUT_RING_STATUS = "stt";
	public static final String PARAM_INPUT_USER_EMAIL = "em";
	public static final String PARAM_INPUT_USER_POLICY = "pl";
	public static final String PARAM_INPUT_USER_REALNAME = "rn";
	
	public static final String PARAM_INPUT_USER_NAME = "u";
	public static final String PARAM_INPUT_PASSWORD = "p";
	public static final String PARAM_INPUT_PHONE = "ph";
	public static final String PARAM_INPUT_DEVICE_TOKEN = "resID";
	public static final String PARAM_INPUT_DEVICE_TOKEN_ID = "tkID";
	public static final String PARAM_INPUT_PAGE = "pg";
	public static final String PARAM_INPUT_REPLY_ID = "repid";
	public static final String PARAM_INPUT_FRIEND_NAME = "fname";
	public static final String PARAM_INPUT_FRIEND_ID = "fid";
	public static final String PARAM_INPUT_KEYWORD = "kw";
	// PARAM_API;
	public static final String URL_LIST_FRIEND = URL_API
			+ PARAM_METHOD_FRIEND_LIST;
	public static final String URL_FRIENDS_OF_FRIEND = URL_API
			+ PARAM_METHOD_FRIENDS_OF_FRIEND;
	public static final String URL_ADD_PARTICIPANTS = URL_API
			+ PARAM_METHOD_ADD_PARTICIPANTS;
	public static final String URL_PARTICIPANT_DELETE = URL_API
			+ PARAM_METHOD_DELETE_PARTICIPANT;
	public static final String URL_LIST_PARTICIPANT = URL_API
			+ PARAM_METHOD_PARTICIPANT_LIST;
	public static final String URL_LIST_GROUP = URL_API
			+ PARAM_METHOD_GROUP_LIST;
	public static final String URL_SIGN_UP = URL_API + PARAM_METHOD_SIGNUP;
	public static final String URL_LOGIN = URL_API + PARAM_METHOD_LOGIN;
	public static final String URL_SET_DEVICE_TOKEN = URL_API
			+ PARAM_METHOD_UPDATE_DEVICE_TOKEN;
	public static final String URL_FRIENDS_REQUEST = URL_API
			+ PARAM_METHOD_FRIENDS_REQUEST;
	public static final String URL_SONG_REQUEST = URL_API
			+ PARAM_METHOD_SONG_REQUEST;
	public static final String URL_FRIEND_REQUEST = URL_API
			+ PARAM_METHOD_FRIEND_REQUEST;
	public static final String URL_FRIEND_ACCEPTED = URL_API
			+ PARAM_METHOD_FRIEND_ACCEPTED;
	public static final String URL_FRIEND_REJECTED = URL_API
			+ PARAM_METHOD_FRIEND_REJECTED;
	public static final String URL_FILE_UPLOAD = URL_API
			+ PARAM_METHOD_UPLOAD_FILE;
	public static final String URL_RING_UPLOAD = URL_API
			+ PARAM_METHOD_UPLOAD_RING;
	public static final String URL_LIST_RING = URL_API
			+ PARAM_METHOD_LIST_RING;
	public static final String URL_LIST_GLOBALRING = URL_API
			+ PARAM_METHOD_LIST_GLOBALRING;
	public static final String URL_LIKE = URL_API
			+ PARAM_METHOD_LIKE;
	public static final String URL_LISTEN = URL_API
			+ PARAM_METHOD_LISTEN;
	public static final String URL_RENAME_GROUP = URL_API
			+ PARAM_METHOD_RENAME_GROUP;
	public static final String URL_DELETE_GROUP = URL_API
			+ PARAM_METHOD_DELETE_GROUP;
	public static final String URL_LEAVE_GROUP = URL_API
			+ PARAM_METHOD_LEAVE_GROUP;
	
	public static final String URL_CREATE_GROUP = URL_API
			+ PARAM_METHOD_CREATE_GROUP;

	public static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}
	
	public static final int CAMERA_REQUEST = 1888; 
	public static final int GALLERY_SELECTION = 1777; 

	public final static boolean isValidEmail(String target) {
		if (target == null) {
			return false;
		} else {
			return android.util.Patterns.EMAIL_ADDRESS.matcher(target)
					.matches();
		}
	}

}
