/*
 * Name: $RCSfile: MyAsyncFileUpload.java,v $
 * Version: $Revision: 1.1 $
 * Date: $Date: Jun 5, 2012 10:09:54 AM $
 *
 * Copyright (C) 2012 COMPANY_NAME, Inc. All rights reserved.
 */
package com.netviet.polibook.net;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.content.Context;

/**
 * MyAsyncFileUpload supports upload babble to server
 *
 * @author Hai Lee
 */
public class MyAsyncFileUpload extends MyAsyncHttpBase {
    protected List<NameValuePair> files;

    /**
     * Constructor
     *
     * @param context
     * @param process
     * @param parameters
     */
    public MyAsyncFileUpload(Context context,
                             MyAsyncHttpResponseProcess process, List<NameValuePair> parameters,
                             List<NameValuePair> files) {
        super(context, process, parameters);
        this.files = files;
    }

    @Override
    protected String request(String url) {
        try {
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters,
                    NETWORK_TIME_OUT);
            HttpClient httpclient = new DefaultHttpClient(httpParameters);
            MultipartEntity entity = new MultipartEntity();

            // Set parameters to form
            if (parameters != null) {
                Iterator<NameValuePair> pit = parameters.iterator();
                while (pit.hasNext()) {
                    NameValuePair nv = pit.next();
                    entity.addPart(nv.getName(), new StringBody(nv.getValue()));
                }
            }

            // Set upload files to form
            if (files != null) {
                Iterator<NameValuePair> fit = files.iterator();
                while (fit.hasNext()) {
                    NameValuePair nv = fit.next();
                    entity.addPart(nv.getName(),
                            new FileBody(new File(nv.getValue())));
                }
            }

            HttpPost httppost = new HttpPost(url);
            httppost.setEntity(new UrlEncodedFormEntity(parameters));
            httppost.setEntity(entity);
            response = httpclient.execute(httppost, httpContext(url));
            statusCode = NETWORK_STATUS_OK;
        } catch (Exception e) {
        	e.printStackTrace();
            statusCode = NETWORK_STATUS_ERROR;
        }
        return null;
    }
}
