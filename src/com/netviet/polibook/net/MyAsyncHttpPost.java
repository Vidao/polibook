/*
 * Name: $RCSfile: AsyncHttpPost.java,v $
 * Version: $Revision: 1.1 $
 * Date: $Date: Apr 21, 2011 2:43:05 PM $
 *
 * Copyright (C) 2011 COMPANY NAME, Inc. All rights reserved.
 */

package com.netviet.polibook.net;

import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.content.Context;

/**
 * AsyncHttpGet makes http post request based on AsyncTask
 * 
 * @author Hai Le
 */
public class MyAsyncHttpPost extends MyAsyncHttpBase
{
    private String className = "";

    /**
     * Constructor
     * 
     * @param context
     * @param listener
     * @param parameters
     */
    public MyAsyncHttpPost(Context context,
        MyAsyncHttpResponseListener listener, List<NameValuePair> parameters)
    {
        super(context, listener, parameters);
        className = context.getClass().toString();
    }

    /**
     * Constructor
     * 
     * @param context
     * @param process
     * @param parameters
     */
    public MyAsyncHttpPost(Context context, MyAsyncHttpResponseProcess process,
        List<NameValuePair> parameters)
    {
        super(context, process, parameters);
        className = context.getClass().toString();
    }

    @Override
    protected String request(String url)
    {
        try
        {
            HttpParams params = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(params, NETWORK_TIME_OUT);
            HttpConnectionParams.setSoTimeout(params, NETWORK_TIME_OUT);
            HttpClient httpclient = createHttpClient(url, params);

            HttpPost httppost = new HttpPost(url);
            if (parameters != null)
            {
                httppost.setEntity(new UrlEncodedFormEntity(parameters));
            }
//            httppost.setHeader("Content-Type", "multipart/form-data");
            response = httpclient.execute(httppost);
            statusCode = NETWORK_STATUS_OK;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            statusCode = NETWORK_STATUS_ERROR;
            
        }
        return null;
    }
}
