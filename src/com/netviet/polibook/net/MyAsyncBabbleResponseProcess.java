/*
 * Name: $RCSfile: MyAsyncBabbleResponseProcess.java,v $
 * Version: $Revision: 1.1 $
 * Date: $Date: Jun 6, 2012 10:03:37 AM $
 *
 * Copyright (C) 2012 COMPANY_NAME, Inc. All rights reserved.
 */
package com.netviet.polibook.net;

/**
 * @author Hai Lee
 */
public interface MyAsyncBabbleResponseProcess {
    void before();

    void after(int statusCode, String path);
}
