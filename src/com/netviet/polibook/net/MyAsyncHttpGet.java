/*
 * Name: $RCSfile: MyAsyncHttpGet.java,v $
 * Version: $Revision: 1.1 $
 * Date: $Date: May 12, 2011 2:38:36 PM $
 *
 * Copyright (C) 2011 COMPANY NAME, Inc. All rights reserved.
 */

package com.netviet.polibook.net;

import java.util.Iterator;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.content.Context;
import android.util.Log;

/**
 * MyAsyncHttpGet makes http get request based on AsyncTask
 *
 * @author Hai Le
 */
public class MyAsyncHttpGet extends MyAsyncHttpBase {
    private String className = "";

    /**
     * Constructor
     *
     * @param context
     * @param listener
     * @param parameters
     */
    public MyAsyncHttpGet(Context context,
                          MyAsyncHttpResponseListener listener, List<NameValuePair> parameters) {
        super(context, listener, parameters);

        this.className = context.getClass().toString();
    }

    @Override
    protected String request(String url) {
        Log.d("GCMLOG", "url = " + url);
        try {
            HttpParams params = new BasicHttpParams();
            if (parameters != null) {
                Iterator<NameValuePair> it = parameters.iterator();
                while (it.hasNext()) {
                    NameValuePair nv = it.next();
                    params.setParameter(nv.getName(), nv.getValue());
                }
            }
            HttpConnectionParams.setConnectionTimeout(params, NETWORK_TIME_OUT);
            HttpConnectionParams.setSoTimeout(params, NETWORK_TIME_OUT);

            HttpClient httpclient = createHttpClient(url, params);
            HttpGet httpget = new HttpGet(url);
            response = httpclient.execute(httpget);
            statusCode = NETWORK_STATUS_OK;
        } catch (Exception e) {
            statusCode = NETWORK_STATUS_ERROR;
        }
        return null;
    }
}
